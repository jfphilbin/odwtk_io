// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library mint_header;

import 'dart:typed_data';

void main() {

   List foo =  "MINT".codeUnits;
   Uint8List magic = new Uint8List.fromList(foo);
   ByteData  bd = magic.buffer.asByteData();
   int number = bd.getUint32(0);
   int m = 'M'.codeUnitAt(0);
   int i = 'I'.codeUnitAt(0);
   int n = 'N'.codeUnitAt(0);
   int t = 'T'.codeUnitAt(0);

   print('0x${m.toRadixString(16)}${i.toRadixString(16)}${n.toRadixString(16)}${t.toRadixString(16)}');
   print(number.toRadixString(16));



}