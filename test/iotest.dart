// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

import 'dart:io';
import 'dart:async';
import 'dart:typed_data';

import 'package:core/core.dart';
import 'package:dictionary/dictionary.dart';

const String MR_FSK = "C:/MICA/git/DCMiD/data/MR_FSK/1.2.250.1.59.453.859.781157385.1640.1290624317.4.1.1.msdicom";

main() {
  read();
}

void read() {
   File file = new File(MR_FSK);
   Future buffer = file.readAsBytes();
       buffer.then((Uint8List bytes) {
        // Uint8List bv = bytes;
         print(bytes);

         //assert(bytes is Uint8List);
         ByteData bdv = new ByteData.view(bytes.buffer);
         int endLength = bdv.lengthInBytes;
         //Read and print magic number
         int offset = 128;
         var s = new Uint8List.view(bdv.buffer, offset, 4);
         print(new String.fromCharCodes(s));
         //read the attributes
         readHeader(bdv.buffer, offset += 4, bdv.lengthInBytes);

         print(bytes);
       });
}
  void readFile() {
    File file = new File(MR_FSK);
    Future <Uint8List> buffer = file.readAsBytes();
        buffer.then((bytes) {
         // Uint8List bv = bytes;
          print(bytes);
          //assert(bytes is Uint8List);
          ByteData bdv = new ByteData.view(bytes.buffer);
          int endLength = bdv.lengthInBytes;
          //Read and print magic number
          int offset = 128;
          var s = new Uint8List.view(bdv.buffer, offset, 4);
          print(new String.fromCharCodes(s));
          //read the attributes

          readHeader(bdv.buffer, offset += 4, bdv.lengthInBytes);

          /*
          //Read and Print 1st attribute
          var group = bdv.getInt16(offset += 4, Endianness.LITTLE_ENDIAN);
          var element = bdv.getInt16(offset += 2, Endianness.LITTLE_ENDIAN);
          int tag = (group << 16) + element;
          var vr = bdv.getInt16(offset += 2, Endianness.LITTLE_ENDIAN);

          offset += 2;
          s = new Uint8List.view(bv.buffer, offset, 2);
          var vr = new String.fromCharCodes(s);
          offset += 2;
          var len = bdv.getInt16(offset, Endianness.LITTLE_ENDIAN);
          //var len = bdv.getInt16(offset);
          offset += 2;
          var vals = new ByteData.view(bv.buffer, offset, len);
          print("Tag= (${group.toRadixString(16)},${element.toRadixString(16)}) VR= $vr, length=$len value=$vals");

          // read 2nd attribute
          offset += len;
          group = bdv.getInt16(offset, Endianness.LITTLE_ENDIAN);
          offset += 2;
          element = bdv.getInt16(offset, Endianness.LITTLE_ENDIAN);
          offset += 2;
          s = new Uint8List.view(bv.buffer, offset, 2);
          vr = new String.fromCharCodes(s);
          offset += 4;
          len = bdv.getInt32(offset, Endianness.LITTLE_ENDIAN);
          offset += 4;
          vals = new ByteData.view(bv.buffer, offset, len);
          print("Tag= (${tag.toRadixString(16)}) VR= $vr, length=$len value=$vals");

          // read 3nd attribute
          offset += len;
          group = bdv.getInt16(offset, Endianness.LITTLE_ENDIAN);
          offset += 2;
          element = bdv.getInt16(offset, Endianness.LITTLE_ENDIAN);
          offset += 2;
          print("Tag= (${group.toRadixString(16)},${element.toRadixString(16)})");
          s = new Uint8List.view(bv.buffer, offset, 2);
          vr = new String.fromCharCodes(s);
          print(vr);
          offset += 2; //?
          len = bdv.getInt16(offset, Endianness.LITTLE_ENDIAN);
          print(len);
          offset += 2;
          vals = new ByteData.view(bv.buffer, offset, len);
          print("Tag= (${group.toRadixString(16)},${element.toRadixString(16)}) VR= $vr, length=$len value=$vals");
          */
       });
  }

  void readHeader(ByteBuffer buffer, int offset, int length) {
    var dataset = new Dataset.top();
    var bdview = new ByteData.view(buffer, offset);
    var group = bdview.getInt16(offset += 4, Endianness.LITTLE_ENDIAN);
    var element = bdview.getInt16(offset += 2, Endianness.LITTLE_ENDIAN);
    int tag = (group << 16) + element;
    int vrCode = bdview.getInt16(offset += 2, Endianness.LITTLE_ENDIAN);
    VR vr = VR.codeToVR(vrCode);
    var values = buffer.readValues(buffer, offset, length, lazy: true);
    dataset.add(tag, vr, values);
  }





