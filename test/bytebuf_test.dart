// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

//DCMiD Toolkit packages
import 'package:unittest/unittest.dart';

import 'package:dictionary/dictionary.dart';
import 'package:io/io.dart';

// Test Utilities
import 'utilities_test.dart';

void main() {

  //Helper functions
  String readString(TestData td) {
    ByteBuf bb = td.toByteBuf;
    return bb.readString(maxLength:td.readLength);
  }

  List readStringList(TestData td) {
    ByteBuf bb = td.toByteBuf;
    return bb.readStringList(td.length, maxItemLength: td.readLength);
  }

  // Basic String tests using the Long String VR (VR.LO)
  TestData BT0 = new TestData("this is just a simple string", VR.LO);
  TestData BT1 = new TestData("this is\\just\\a simple\\string", VR.LO);
  TestData BT2 = new TestData("", VR.LO);
  test('Basic Test', () {
    expect(readString(BT0), equals(BT0.string));
    expect(readStringList(BT1), equals(BT1.toList));
    expect(readString(BT2), equals(BT2.string));
    expect(readStringList(BT2), equals(BT2.toList));
  });

  //AETitle = VR.AE
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData AE0 = new TestData("ab1", VR.AE);
  TestData AE1 = new TestData("0123456789012345", VR.AE); // 16 chars
  TestData AE2 = new TestData("", VR.AE); // 16 chars
  //TODO
  //Fail
  test('Read VR.AE', () {
    print(VR.AE);
    expect(readStringList(AE0), equals(AE0.toList));
    expect(readStringList(AE1), equals(AE1.toList));
    expect(readStringList(AE2), equals(AE2.toList));
    //TODO Fail
  });

  //Age = VR.AS
  //VFSize = 2, MaxLength = 4, Multiple Values = N
  TestData AS0 = new TestData("012D", VR.AS);
  TestData AS1 = new TestData("123W", VR.AS);
  TestData AS2 = new TestData("765M", VR.AS);
  TestData AS3 = new TestData("987Y", VR.AS);
  test('Read VR.AS', () {
    print(VR.AS);
    expect(readString(AS0), equals(AS0.string));
    expect(readString(AS1), equals(AS1.string));
    expect(readString(AS2), equals(AS2.string));
    expect(readString(AS3), equals(AS3.string));
  });

  //Code String = VR.CS
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData CS0 = new TestData("ab1", VR.CS);
  TestData CS1 = new TestData("01234567\\89012345", VR.CS); // 16 chars
  test('Read VR.CS', () {
    print(VR.CS);
    expect(readStringList(CS0), equals(CS0.toList));
    expect(readStringList(CS1), equals(CS1.toList));
  });

  //Date = VR.DA
  //VFSize = 2, MaxLength = 8, Multiple Values = Y
  TestData DA0 = new TestData("20140506", VR.DA);
  TestData DA1 = new TestData("19500718\\19520102\\19931118\\19960131", VR.DA); // 16 chars
  test('Read VR.DA', () {
    print(VR.DA);
    expect(readStringList(DA0), equals(DA0.toList));
    expect(readStringList(DA1), equals(DA1.toList));
  });

  //Digital String = VR.DS
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData DS0 = new TestData("0.0", VR.DT);
  TestData DS1 = new TestData("1.1", VR.DS);
  TestData DS2 = new TestData("01234567.8901234", VR.DS); // 16 chars
  TestData DS3 = new TestData(".12345", VR.DS);
  TestData DS4 = new TestData("01234567.8901234", VR.DS); // 16 chars
  TestData DS5 = new TestData("-1.1", VR.DS);
  TestData DS6 = new TestData("+1234567.8901234", VR.DS); // 16 chars
  TestData DS7 = new TestData(".12345E10", VR.DS);
  TestData DS8 = new TestData("01234567.901e345", VR.DS); // 16 chars
  // Fail
  TestData NDS0 = new TestData("", VR.DS); // 0 chars
  TestData NDS1 = new TestData("\\", VR.DS); // 0 chars
  TestData NDS2 = new TestData("01234567.89012345", VR.DS); // 17 chars
  TestData NDS3 = new TestData("+/-", VR.DS); // 0 chars
  TestData NDS4 = new TestData("01234567.89A12345", VR.DS); // 17 chars
  test('Read VR.DS', () {
    print(VR.DS);
    expect(readStringList(DS0), equals(DS0.toList));
    expect(readStringList(DS1), equals(DS1.toList));
    expect(readStringList(DS2), equals(DS2.toList));
    expect(readStringList(DS3), equals(DS3.toList));
    expect(readStringList(DS4), equals(DS4.toList));
    expect(readStringList(DS5), equals(DS5.toList));
    expect(readStringList(DS6), equals(DS6.toList));
    expect(readStringList(DS7), equals(DS7.toList));
    expect(readStringList(DS8), equals(DS8.toList));
    //Fail
    expect(readStringList(NDS0), equals(NDS0.toList));
    expect(readStringList(NDS1), equals(NDS1.toList));
    expect(readStringList(NDS2), equals(NDS2.toList));
    expect(readStringList(NDS3), equals(NDS3.toList));
  });

  //Age = VR.DT
  //VFSize = 2, MaxLength = 26, Multiple Values = Y
  TestData DT0 = new TestData("19501231235959", VR.AS);   // No ms or tz
  TestData DT1 = new TestData("20140505000000.123456", VR.AS); //full
  TestData DT2 = new TestData("20130405010203.123456+0130", VR.AS);
  TestData DT3 = new TestData("19500505003030.123-0245", VR.AS);
  //Fail
  TestData NDT0 = new TestData("19501231235959", VR.AS);   // No ms or tz
  TestData NDT1 = new TestData("20140505000000.123456", VR.AS); //full
  TestData NDT2 = new TestData("20130405010203.123456+0130", VR.AS);
  TestData NDT3 = new TestData("19500505003030.123-0245", VR.AS);
  //TODO
  test('Read VR.DT', () {
    print(VR.DT);
    expect(readStringList(DT0), equals(DT0.toList));
    expect(readStringList(DT1), equals(DT1.toList));
    expect(readStringList(DT2), equals(DT2.toList));
    expect(readStringList(DT3), equals(DT3.toList));
    //Fail
    expect(readStringList(NDT0), equals(NDT0.toList));
    expect(readStringList(NDT1), equals(NDT1.toList));
    expect(readStringList(NDT2), equals(NDT2.toList));
    expect(readStringList(NDT3), equals(NDT3.toList));
  });
/*
  //Code String = VR.IS
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData aSingleCodeString = new TestData("ab1", VR.CS);
  TestData aCoupleOfCodeStrings = new TestData("01234567\\89012345", VR.CS); // 16 chars
  test('Read VR.CS', () {
    print(VR.CS);
    expect(readStringList(aSingleCodeString), equals(aSingleCodeString.toList));
    expect(readStringList(aCoupleOfCodeStrings), equals(aCoupleOfCodeStrings.toList));
  });

  //Date = VR.LO
  //VFSize = 2, MaxLength = 8, Multiple Values = Y
  TestData aDate = new TestData("20140506", VR.DA);
  TestData someDates = new TestData("19500718\\19520102\\19931118\\19960131", VR.DA); // 16 chars
  test('Read VR.DA', () {
    print(VR.DA);
    expect(readStringList(aDate), equals(aDate.toList));
    expect(readStringList(someDates), equals(someDates.toList));
  });

  //AETitle = VR.LT
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData aSmallAETitle = new TestData("ab1", VR.LT);
  TestData aBigAETitle = new TestData("0123456789012345", VR.LT); // 16 chars
  TestData aNonAETitle = new TestData("", VR.LT); // 16 chars
  test('Read VR.LT', () {
    print(VR.LT);
    expect(readStringList(aSmallAETitle), equals(aSmallAETitle.toList));
    expect(readStringList(aBigAETitle), equals(aBigAETitle.toList));
    expect(readStringList(aNonAETitle), equals(aNonAETitle.toList));
  });

  //Age = VR.PN
  //VFSize = 2, MaxLength = 4, Multiple Values = N
  TestData aVeryYoungAge = new TestData("012D", VR.AS);
  TestData aYoungAge = new TestData("123W", VR.AS);
  TestData anOlderAge = new TestData("765M", VR.AS);
  TestData aVeryOldAge = new TestData("987Y", VR.AS);
  solo_test('Read VR.AS', () {
    print(VR.AS);
    expect(readStringList(aVeryYoungAge), equals(aVeryYoungAge.toList));
    expect(readStringList(aYoungAge), equals(aYoungAge.toList));
    expect(readStringList(anOlderAge), equals(anOlderAge.toList));
    expect(readStringList(aVeryOldAge), equals(aVeryOldAge.toList));
  });

  //Code String = VR.SH
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData aSingleCodeString = new TestData("ab1", VR.CS);
  TestData aCoupleOfCodeStrings = new TestData("01234567\\89012345", VR.CS); // 16 chars
  test('Read VR.CS', () {
    print(VR.CS);
    expect(readStringList(aSingleCodeString), equals(aSingleCodeString.toList));
    expect(readStringList(aCoupleOfCodeStrings), equals(aCoupleOfCodeStrings.toList));
  });

  //Date = VR.ST
  //VFSize = 2, MaxLength = 8, Multiple Values = Y
  TestData aDate = new TestData("20140506", VR.DA);
  TestData someDates = new TestData("19500718\\19520102\\19931118\\19960131", VR.DA); // 16 chars
  test('Read VR.DA', () {
    print(VR.DA);
    expect(readStringList(aDate), equals(aDate.toList));
    expect(readStringList(someDates), equals(someDates.toList));
  });

  //AETitle = VR.TM
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData aSmallAETitle = new TestData("ab1", VR.TM);
  TestData aBigAETitle = new TestData("0123456789012345", VR.TM); // 16 chars
  TestData aNonAETitle = new TestData("", VR.TM); // 16 chars
  test('Read VR.TM', () {
    print(VR.TM);
    expect(readStringList(aSmallAETitle), equals(aSmallAETitle.toList));
    expect(readStringList(aBigAETitle), equals(aBigAETitle.toList));
    expect(readStringList(aNonAETitle), equals(aNonAETitle.toList));
  });

  //Age = VR.UI
  //VFSize = 2, MaxLength = 4, Multiple Values = N
  TestData aVeryYoungAge = new TestData("012D", VR.AS);
  TestData aYoungAge = new TestData("123W", VR.AS);
  TestData anOlderAge = new TestData("765M", VR.AS);
  TestData aVeryOldAge = new TestData("987Y", VR.AS);
  solo_test('Read VR.AS', () {
    print(VR.AS);
    expect(readStringList(aVeryYoungAge), equals(aVeryYoungAge.toList));
    expect(readStringList(aYoungAge), equals(aYoungAge.toList));
    expect(readStringList(anOlderAge), equals(anOlderAge.toList));
    expect(readStringList(aVeryOldAge), equals(aVeryOldAge.toList));
  });

  //Code String = VR.UR
  //VFSize = 2, MaxLength = 16, Multiple Values = Y
  TestData aSingleCodeString = new TestData("ab1", VR.CS);
  TestData aCoupleOfCodeStrings = new TestData("01234567\\89012345", VR.CS); // 16 chars
  test('Read VR.CS', () {
    print(VR.CS);
    expect(readStringList(aSingleCodeString), equals(aSingleCodeString.toList));
    expect(readStringList(aCoupleOfCodeStrings), equals(aCoupleOfCodeStrings.toList));
  });

  //Date = VR.UT
  //VFSize = 4, MaxLength = 10, Multiple Values = N
  TestData aDate = new TestData("20140506", VR.DA);
  TestData someDates = new TestData("19500718\\19520102\\19931118\\19960131", VR.DA); // 16 chars
  test('Read VR.DA', () {
    print(VR.DA);
    expect(readStringList(aDate), equals(aDate.toList));
    expect(readStringList(someDates), equals(someDates.toList));
  });
  */

}

/*
String s1234 = "1234";
Uint8List b1234 = s1234.codeUnits;
int i1234 = 1234;
String sList = "abcd\\efgh\\ijkl\\mnop";
ByteBuf bbStrList = stringToBB(sList);

String tagToHex(int tag) {
  String s = tag.toRadixString(16);
  while (s.length < 8) s = "0" + s;
  return "0x" + s;
}

ByteBuf stringToBB(String s) {
  Uint8List bytes = new Uint8List.fromList(s.codeUnits);
  return new ByteBuf.fromUint8List(bytes);
}

Uint8List toUint8List(var s) {
  return new Uint8List.fromList(s.toString().codeUnits);
}

Uint8List attrToBB(int tag, VR vr, var value) {
  String t = tagToHex(tag);
  String v = value.toString();
  int len = v.length;
  int maxLen = vr.length;
  ByteData bd;


}
*/

/*
test('Uint8Test', () {
  expect(wrRdUint8(0), 1);
  expect(wrRdUint8(1), 1);
});

BBStr sd = new BBStr();
print("fiveBB=${sd.fiveBB}");
String result = sd.tenOfFiveBB.readString(delimiter: -1);
print("result0=$result, length=${result.length}");
result = sd.tenOfFiveBB.readString(maxLength: 50);
print("result1=$result, length=${result.length}");
List list = sd.tenOfFiveBB.readStringList(-1, 16);
print("result2=$list, length=${list.length}");

/*
print(aString.length);
String test = aString + aStringLength.toString() + s1234;
print(test.length);
print("type=${bytes.runtimeType} $bytes");
ByteBuf bb = stringToBB(test);
String s0 = bb.readString(39);
print(s0);
String s1 = bb.readString(6);
print(s1);
List<String> l1 = bbStrList.readStringList(sList.length, 4);
print(l1);
//readIntegerTest();
 *
 */
print("Starting ByteBuf testing ...");
test("ByteBuf.readString", testString);
 */

/*
const String empty = "";
const String one = "1";
const String five = "zyxwv";
const String tenOfFive
    = "aaaaa\\bbbbb\\ccccc\\ddddd\\eeeee\\fffff\\ggggg\\hhhhh\\iiiii\\jjjjj";
const String tenOfOneToTen
    = "1\\12\\123\\1234\\12345\\123456\\1234567\\12345678\\123456789\\1234567890";

class BBStrings {
  String s;

  BBStrings(this.s);

  ByteBuf oneBB = new BBStrings(one);
  ByteBuf fiveBB = new BBStrings(five);
  ByteBuf tenOfFiveBB = new BBStrings(tenOfFive);
  ByteBuf tenOfOneToTenBB = new BBStrings(tenOfOneToTen);
  ByteBuf emptyBB = new BBStrings(empty);
}

class BBInt {
  // good cases
  static const String one ="1";
  static const String plusOne = "+1";
  static const String minusOne = "-1";
  static const String eleven = "12345678901";
  static const String elevenPlus = "+12345678901";
  static const String elevenMinus = "-12345678901";

  ByteBuf oneBB = stringToBB(one);
  ByteBuf plusOneBB = stringToBB(plusOne);
  ByteBuf minusOneBB = stringToBB(minusOne);
  ByteBuf elevenBB = stringToBB(eleven);
  ByteBuf elevenPlusBB = stringToBB(elevenPlus);
  sByteBuf elevenMinusBB = stringToBB(elevenMinus);

  //errors
  static const String bad0 = "";
  static const String bad1 = "1234abcd";
  static const String bad2 = "123d4";
  static const String bad3 = "0x12AB3F";  // Hex is not legal in IS
  static const String bad4 = "1234+";
  static const String bad5 = "123-4";
  static const String bad13 = "-1234567890123";

  ByteBuf bad0BB = stringToBB(bad0);
  ByteBuf bad1BB = stringToBB(bad1);
  ByteBuf bad2BB = stringToBB(bad2);
  ByteBuf bad3BB = stringToBB(bad3);
  ByteBuf bad4BB = stringToBB(bad4);
  ByteBuf bad5BB = stringToBB(bad5);
  ByteBuf bad13BB = stringToBB(bad13);

}


bool testString() {
  String empty = "";
  String one = "1";
  String five = "zyxwv";
  String tenOfFive
      = "aaaaa\\bbbbb\\ccccc\\ddddd\\eeeee\\fffff\\ggggg\\hhhhh\\iiiii\\jjjjj";
  String tenOfOneToTen
      = "1\\12\\123\\1234\\12345\\123456\\1234567\\12345678\\123456789\\1234567890";

  //TODO what to do here?
  //ByteBuf emptyBB = stringToByteBuf(empty);
  ByteBuf oneBB = stringToBB(one);
  ByteBuf fiveBB = stringToBB(five);
  ByteBuf tenOfFiveBB = stringToBB(empty);
  ByteBuf tenOfOneToTenBB = stringToBB(empty);

  //String s = sd.oneBB.readString(1);
  print(s);
  assert(one == oneBB.readString());
  /*
  group("ByteBuff.readString tests...", () => {
    test('readString one', () => {
        String s = sd.oneBB.readString(1);
        print(s);
        expect(s, equals(one))
    });
    test('readString five', () =>
        expect(fiveBB.readString(5), five));
    test('readString tenOfFive', () =>
        expect(tenOfFiveBB.readString(-1), tenOfFive));
  });
  */

}

readIntegerTest() {
  String s1 = "  -12345  \\ +987\\ 432";
  String s2 = ".54321";
  double n2 = .54321;
  String s3 = "E-9876";
  String s4 = "1234.56789E-2";
  /*
  ValueField vf1 = new ValueField(uint8(s1));
  List list = readIntegerStringList(vf1);
  print("s1= $s1, list=$list");
  ValueField vf2 = new ValueField(uint8(s2));
  double f = readFractionalPart(vf2);
  print("s2=$s2,  fraction=$f");
  ValueField vf3 = new ValueField(uint8(s3));
  int exp = readExponentPart(vf3);
  print("s3=$s3, Exp=$exp");
  */
  //print(s4);
  //ValueField vf4 = new ValueField(uint8(s4));
  //double val = readDecimalNumber(vf4);
  //print("s3=$s4, val=$val");
  //int j = int.parse(s2);
  //int k = int.parse(s3);

  //print("s= $s2, j=$j");
  //print("s= $s3, k=$k");
}

int wrRdUint8(int i) {
  ByteBuf bb = new ByteBuf.empty(8);
  bb.writeUint8(i);
  return bb.readUint8();
}
*/


