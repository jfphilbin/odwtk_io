
import 'dart:convert';
import 'dart:io';
import 'dart:typed_data';

//import 'package:utilities/utilities.dart';
import 'package:dictionary/dictionary.dart';
import 'package:core/core.dart';
import 'package:io/io.dart';

void printAttr(int tag, VR vr, int length, values) {
  var t = tagToDcmFmt(tag);
  var v = vr.symbol;

  print('$t $v [$length]:$values');
}

void readAttribute(reader) {
  int tag = reader.readTag();;
  VR vr = reader.readVR();
  int vfLength = reader.readVFLength(vr.isLarge);
  //int vfLimit = reader.getVFLimit(vfLength);
  var values = reader.uInt32List(vfLength);
  printAttr(tag, vr, vfLength, values);
}

class Attribute {
  int tag;
  VR  vr;
  int vfLength;
  var values;

  Attribute(this.tag, this.vr, this.vfLength, this.values);

  String toString() {
    var t = tagToDcmFmt(tag);
    var v = vr.symbol;
    return '$t $v <$vfLength>:$values';
  }

}

void readFunnyTag(DatasetReader reader, int tag) {
  print('FunnyTag=$tag, ${tagToDcmFmt(tag)}');
  int length = reader.readUint32();
  var bytes = reader.uInt8List(length);
  print('tag=${tagToHex(tag)}, length=$length, $bytes');
  readAttr(reader);
}

Attribute readAttrIVR(DatasetReader reader) {
  // reader.debug(0, 16);
  int tag = reader.readTag();
  print('tag=${tagToDcmFmt(tag)}');

  //TODO next line
  //VR vr = tagToVRMap(tag);
  //print('$vr ${vr.name}');
  var values;
  int vfLength = reader.readVFLength(true);
  print('vfLength=$vfLength');
  int currentIndex = reader.index;
  VR vr = tagToVRMap(tag);
  if (vr == VR.SQ) {
    values = readSequence(reader, vfLength);
  } else {
    int vfLimit = reader.getVFLimit(vfLength);
    print('vfLength=$vfLength, vfLimit=$vfLimit');
    values = reader.readSimpleValue(tag, vr, vfLength);
  }
  var attr = new Attribute(tag, vr, vfLength, values);
  print(attr.toString());
  return attr;
}
Attribute readAttrXVR(DatasetReader reader) {
  // reader.debug(0, 16);
  int tag = reader.readTag();
  print('tag=${tagToDcmFmt(tag)}');
  //if (tag == 0x00080000) readFunnyTag(reader, tag);
  VR  vr = reader.readVR();
  print('$vr ${vr.name}');
  var values;
  int vfLength = reader.readVFLength(vr.isLarge);
  print('vfLength=$vfLength');
  int currentIndex = reader.index;
  if (vr == VR.SQ) {
    values = readSequence(reader, vfLength);
  } else {
    int vfLimit = reader.getVFLimit(vfLength);
    print('vfLength=$vfLength, vfLimit=$vfLimit');
    values = reader.readSimpleValue(tag, vr, vfLength);
  }
  var attr = new Attribute(tag, vr, vfLength, values);
  print(attr.toString());
  return attr;
}

List readSequence(DatasetReader reader, int vfLength) {
  print('in readSequence');
  List seq = new List();
  seq.add("Sequence");
  seq.add(-1);
  int start = reader.index;

  if (vfLength > -1) {
    while (vfLength > 0) {
      seq.add(readItem(reader));
      //dsCurrent.add(tag, vr, readSequence(tag, vr, vfLimit));
      vfLength = vfLength - seq[1];
    }
  } else if (vfLength == -1) {
    error('Length = -1');
      //dsCurrent.add(tag, vr, readDelimitedSequence(tag, vr));
  }
  int end = reader.index;
  if ((end - start) != vfLength) {
    error('Length Error: vfLength = $vfLength, start = $start, end=$end, end-start = ${end - start}');
  }
  return seq;
  /*
  else {
    error('invalid vfLength = $vfLength');
  //} else
    //dsCurrent.add(tag, vr, readSimpleValue(tag, vr, vfLimit));
  */
}

List readItem(reader) {
  List item = new List();
  item.add('Item');
  int vfLength = reader.readVFLength(true);
  item.add(vfLength);

  if (vfLength > -1) {
    while (vfLength > 0) {
      item.add(readAttribute(reader));
      //dsCurrent.add(tag, vr, readSequence(tag, vr, vfLimit));
      vfLength = vfLength - item[1];
    }
  } else if (vfLength == -1) {
    error('Length = -1');
      //dsCurrent.add(tag, vr, readDelimitedSequence(tag, vr));
  } else {
    error('invalid vfLength = $vfLength');
  }
  //else dsCurrent.add(tag, vr, readSimpleValue(tag, vr, vfLimit));
  return item;
}

void main() {
  String path = 'D:/acr_sfd_data/334/0000000C';
  File file = new File(path);
  Uint8List bytes = file.readAsBytesSync();
  var fmi = FileMetaInfo.maybeRead(bytes);
  print(fmi);
  /*
  DatasetReader test = datasetReaderFromPathSync(path);
  print(test.uInt8List(128));
  print(ASCII.decode(test.uInt8List(4)));
  test.seek(128);
  print(test.readUint32());

  DatasetReader dr = datasetReaderFromPathSync(path);
  var fmi = FileMetaInfo.maybeRead(dr);
  print(fmi);
  print(0x4449434d);
   */
}