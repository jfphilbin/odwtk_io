// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>

import 'package:core/core.dart';
//import 'package:io/dataset_reader.dart';
import 'package:io/file_io.dart';



String file0
    = "../data/MR_FSK/1.2.250.1.59.453.859.781157385.1640.1290624317.4.1.1.msdicom";

void main() {

  Dataset ds = readMetadataFromFileSync(file0);
  print(ds);
}