**Imaging 3.0**
##DWDK\* FileIO

This repository contains the *FileIO* package. It contains classes used to read and write *Media Types* to files.


<sup>\*</sup>DICOMweb<sup><small>TM</small></sup> Toolkit