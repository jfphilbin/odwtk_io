// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library mint_header;

// Similar to the edu.jhmi.mint.io package

//TODO add comments, format, and cleanup inline commments

import 'dart:typed_data';

import 'package:io/typed_data_reader.dart';
import 'package:io/typed_data_writer.dart';
import 'package:utilities/uuid.dart';



class PayloadType {
  final String name;

  const PayloadType(this.name);

  static const METADATA = const PayloadType("Metadata");
  static const BULKDATA = const PayloadType("Bulkdata");
}

//TODO replace with integer
const int MAGIC = 0x4d494e54;  //int hex  M= 0x4d, I=ox49, N=0x4E, T=0x54

// Option Flags
const int bulkdataFlag   = 0x0001;
const int uuidFlag       = 0x0002;
const int uidFlag        = 0x0004;
const int compressedFlag = 0x0008;
const int encryptedFlag  = 0x0010;
const int ivFlag         = 0x0020;
const int MAX_FLAG       = ivFlag;

// Version Numbers
const int MAJOR_VERSION  = 2;
const int MINOR_VERSION   = 1;

//TODO do we need these?
const int         PEEK_LENGTH = 8;   // Magic, Version and Length
const int         MIN_LENGTH = 22;   // All required fields
const int         MAX_LENGTH = 120;  // All required fields plus all options

class MintHeader {
    // All of these fields aren't necessary, but they help with debugging
    int         magic;
    int         majorVersion;
    int         minorVersion;
    int         optionsLength;
    int         payloadLength;
    int         optionsFlags;
    PayloadType type;
    Uuid        metadataUuid;
    Uuid        fileUuid;
    //TODO make this be a valid UID
    String      studyUID;
    String      compressionName;
    String      encryptionName;
    Uint8List   ivValue;

    MintHeader.empty();

    MintHeader(this.magic, this.majorVersion, this.minorVersion, this.optionsLength,
                   this.payloadLength, this.optionsFlags, this.type, this.metadataUuid,
                   this.fileUuid, this.studyUID, this.compressionName, this.encryptionName,
                   this.ivValue);

    // Predicates
    //TODO make these private
    static bool isBulkdata(int optionsFlags)    => (optionsFlags & bulkdataFlag) == 1;
    static bool hasUuid(int optionsFlags)       => (optionsFlags & uuidFlag) == 1;
    static bool hasUid(int optionsFlags)        => (optionsFlags & uidFlag) == 1;
    static bool isCompressed(int optionsFlags) => (optionsFlags & compressedFlag) == 1;
    static bool isEncrypted(int optionsFlags)   => (optionsFlags & encryptedFlag) == 1;

  /**
   * Read a MINT Header from a [buffer].
   *
   * Returns [null] if the [buffer] does not contain a MINT Header.
   */
  MintHeader MaybeReadMintHeader(TypedDataReader rd) {
     int length;
     MintHeader mh = new MintHeader.empty();

     mh.magic = rd.readUint32();
     if (mh.magic != MAGIC) return null;
     mh.majorVersion = rd.uInt8();
     if (mh.majorVersion != MAJOR_VERSION) readError('wrong Major Version Number $majorVersion');
     mh.minorVersion = rd.uInt8();
     if (mh.minorVersion != MINOR_VERSION) readError('wrong Minor Version Number $minorVersion');
     mh.payloadLength = rd.readUint32();
     mh.optionsLength = rd.readUint16();
     mh.optionsFlags  = rd.readUint32();

     // Read the Options
     if (isBulkdata(optionsFlags)) {
       mh.type = PayloadType.BULKDATA;
       length = rd.readUint16();
       mh.metadataUuid = rd.readUuid();
     }
     if (hasUuid(optionsFlags)) {
       length = rd.readUint16();
       mh.fileUuid = rd.readUuid();
     }
     if (hasUid(optionsFlags)) {
       length = rd.readUint16();
       mh.studyUID = rd.readUid();
     }
     if (isCompressed(optionsFlags)) {
       length = rd.readUint16();
       String compressionName = rd.readString(length);
     }
     if (isEncrypted(optionsFlags)) {
       length = rd.readUint16();
       String encryptionName = rd.readString(length);
       length = rd.readUint16();
       Uint8List iv = rd.uInt8List(length);
     }
     return mh;
   }

  static void writeMintHeader(MintHeader mh, [TypedDataWriter wb]) {
    wb.writeUint32(MAGIC);
    wb.writeUint8(MAJOR_VERSION);
    wb.writeUint8(MINOR_VERSION);
    int beginOptions = wb.index;
    //TODO finish
  }

  //TODO move to io_errors.dart
  void readError(String msg) => parseError(msg);
}


