
library mint_header_option;


import 'dart:typed_data';
import 'dart:convert';

/**
 * MINT Header
 *
 *  magis:   4 bytes that identify the MINT Header format. The value shall be “MINT” encoded in ASCII.

 *  version: The version number of the MINT Header format in use.  The first byte represents the
 *           major version, the second byte the minor version. Implementations that conform to the
 *           format defined by this document shall set the value of this field to {0x02, 0x00},
 *           i.e. MINT 2.0
 *  HdrLen   2 bytes the specify the length of the header, in bytes. This length includes
 *           the 4 byte Option Flags field if any Option fields are present.
 *  length   int32 indicating the length of the uncompressed payload
 *  Option Flags:  4 byte bitmap (containing 32 bits) in which each bit encodes a property of
 *                 the payload. Currently, the following bits are defined:
 *       0x0001: If set, the payload is MINT Bulkdata; otherwise, the payload is MINT Metadata.
 *                The value of the Bulkdata option is the UUID of the Metadata that created it.
 *       0x0002: “Compressed” flag. If set, the payload is compressed; otherwise, the payload is
 *               not compressed.  The value of the Compressed option is the name of the compression
 *               algorithm used.
 *       0x0004: “Uncompressed Length” flag. If set, the value of this option is the uncompressed
 *               length of the payload.
 *       0x0008: “Encrypted” flag. If set, the payload is encrypted; otherwise, the payload is not
 *               encrypted.  The value of this option is the name of the algorithm used to encrypt
 *               the payload.
 *       0x0010: “IV” flag. If set, the IV Option is present; otherwise, the IV Option is not
 *               present.  This value of this option is the value of the IV.
 *       0x0020: “UID” flag. If set, the UID Option is present; otherwise, the UID Option is
 *               not present.  The value of this option is an ascii string containing the UID.
 *       0x0040: “UUID” flag. If set, the UUID Option is present; otherwise, the UUID Option is
 *               not present.  The value of this option is the 16 byte UUID that is used as the
 *               name of this object.
 *
 *  Header Payload: Each option value has a 2 byte length, followed by the value, which that number
 *               of bytes long
 *  Object Payload: The object (metadata or bulkdata) contained in the file.
 */

class MintHeader {
  // Magic bytes that identify the MINT Header format. The value shall be “MINT” encoded in ASCII MINT=[77, 73, 78, 84].
  static       Uint8List MAGIC_AsList = [77, 73, 78, 84];
  static const int       MAGIC_AsInt  = 1296649812;
  // Header Flags
  static const int BULKDATA_FLAG             = 0x00000001;
  static const int COMPRESSED_FLAG           = 0x00000002;
  static const int ENCRYPTED_FLAG            = 0x00000004;
  static const int IV_FLAG                   = 0x00000008;
  static const int UID_FLAG                  = 0x00000010;
  static const int UUID_FLAG                 = 0x00000020;

  static const int MAJOR_VERSION = 0;        //
  static const int MINOR_VERSION = 1;
  static const int VERSION = MAJOR_VERSION << 16 + MINOR_VERSION;

  int       magic;          // MINT magic number
  int       majorVersion;   // The major version number (1 bytes)
  int       minorVersion;   // The minor version number (1 bytes)
  int       headerLength;   // The length of the header (2 bytes)
  int       payloadLength;  // The length of the UNCOMPRESSED payload (4 bytes)
  int       headerFlags;    // 32 bits of flags
  Uint8List bulkdataUuid;
  String    compressionAlgorithm;
  String    encryptionAlgorithm;
  Uint8List iv;
  String    uid;
  Uint8List uuid;
  // temp variables
  Uint8List bytes;
  ByteData  bd;
  int       _index = 0;

  // Getters
  bool get isBulkdata   => _isFlagOn(BULKDATA_FLAG);
  bool get isCompressed => _isFlagOn(COMPRESSED_FLAG);
  bool get isEncrypted  => _isFlagOn(ENCRYPTED_FLAG);
  bool get hasIV        => _isFlagOn(IV_FLAG);
  bool get hasUid       => _isFlagOn(UID_FLAG);
  bool get hasUuid      => _isFlagOn(UUID_FLAG);
  bool _isFlagOn(int flag) => flag & headerFlags == 1;

  // Constructor
  MintHeader(this.bytes) {
    bd = bytes.buffer.asByteData();
    _checkMagic(bd);
    read();
  }

  static bool isMintHeader(header) {
    ByteData bd;
    if (header is ByteData) {
      bd = header;
    } else if (header is Uint8List) {
      bd = header.buffer.asByteData();
    } else throw new TypeError();
    int magic = bd.getUint32(0);
    return magic != MAGIC_AsInt;
  }

  void _checkMagic(header) {
     if (!isMintHeader(header)) throw new ArgumentError('Not a MINT Header');
  }

  MintHeader read() {
    majorVersion  = bd.getUint8(4);
    minorVersion  = bd.getUint8(5);
    headerLength  = bd.getUint16(6);
    payloadLength = bd.getUint32(8);
    headerFlags   = bd.getUint32(12);

    int _index = 16;
    if (isBulkdata)   bulkdataUuid         = readBytes();
    if (isCompressed) compressionAlgorithm = readString();
    if (isEncrypted ) encryptionAlgorithm  = readString();
    if (hasIV)        iv                   = readBytes();
    if (hasUid )      uid                  = readString();
    if (hasUuid)      uuid                 = readBytes();
    return this;
  }

  Uint8List readBytes() {
    int length = bd.getUint16(_index);
    Uint8List field = bytes.sublist(_index + 2, length);
    _index += 2 + length;
    return field;
  }

  String readString() => UTF8.decode(readBytes());

  Uint8List write(Uint8List bytes) {

    bd.setUint32(0, MAGIC_AsInt);
    bd.setUint8(4, majorVersion);
    bd.setUint8(5, minorVersion);
    bd.setUint32(8, payloadLength);
    bd.setUint32(12, headerFlags);

    int _index = 16;
    if (isBulkdata)    writeBytes(bulkdataUuid);
    if (isCompressed)  writeString(compressionAlgorithm);
    if (isEncrypted )  writeString(encryptionAlgorithm);
    if (iv   != null)  writeBytes(iv);
    if (uid  != null ) writeString(uid);
    if (uuid != null)   writeBytes(uuid);

    bd.setUint16(6, headerLength);
    return bytes;

  }

  void writeBytes(Uint8List bytes) {
    int length = bytes.length;
    bd.setUint16(_index, length);
    int end = _index + length;
    bytes.setRange(_index + 2, end, bytes);
    _index += 2 + length;

  }

  String writeString(String s) { writeBytes(s.codeUnits); }
}

  /*

  //TODO need BDBuilder
  void encode(ByteData bd) {
    //TODO what should offset be
    int offset = 0;
    bd.setUint16(0, length & 0xFF);
    encodeValue(bd);
  }

  void encodeValue(ByteData bd);

  void decode(ByteData bd) {
    //TODO what is write offset?  It is 0 below
    int length = bd.getUint16(0) & 0xFF;  // Length (2 bytes)
    decodeValue(bd, length);
    }

  void decodeValue(ByteData bd, int length);

   // @Override
    int hashCode() {
      final int prime = 31;
      int result = 1;
      return prime * result + (value == null ? 0 : value.hashCode());
    }

    //@Override
    bool equals(Object obj) {
      if (this == obj)  return true;
      if (!(obj is MintHeaderOption)) return false;
      MintHeaderOption other = obj;
      if (value == null) other.value == null;
      return value.equals(other.value);
    }
}


class MintIVOption extends MintHeaderOption<Uint8List> {
  final int _IV_SIZE = 12;

  MintIVOption(Uint8List ivBytes) {
    setValue(ivBytes);
  }

  @override
  void setValue(Uint8List value) {
    if (value.length != IV_SIZE) {
      throw new ArgumentError("IV must be $IV_SIZE bytes");
    }
    setValue(value);
  }

  @override
  int getBitmask() {
    return MintHeaderOption.BITMASK_IV;
  }

  @override
  int calculateLength => IV_SIZE;


  @override
  protected void encodeValue(ByteData bd) {
    Uint8List ivBytes = this.getValue();
    bd.setUint8List(ivBytes);
  }

  @Override
  protected void decodeValue(ByteData bd, int length) {
    Uint8List ivBytes = new byte[IV_SIZE];
    bd.get(ivBytes);
    setValue(ivBytes);
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) return true;
    if (!(obj instanceof MintIVOption)) return false;
    Uint8List value = this.getValue();
    Uint8List otherValue = ((MintIVOption) obj).getValue();
    if (value == null) return otherValue == null;
    return Arrays.equals(value, otherValue);
    }
}

*/