// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library io.error;

// Error


class ParseError extends Error {
  final message;
  ParseError(this.message);
}

class ReadError extends Error {
  final message;

  ReadError(this.message);
}

class WriteError extends Error {
  final message;
  WriteError(this.message);
}

// Errors
ArgumentError argumentError(String msg) => throw new ArgumentError(msg);
ArgumentError emptyBufferError(String msg) => throw new ArgumentError(msg);
ParseError     parseError(String msg) => throw new ParseError(msg);
ReadError      readError(String msg) => throw new ReadError(msg);
RangeError    rangeError(String msg) => throw new RangeError(msg);
ReadError     writeError(String msg) => throw new WriteError(msg);

