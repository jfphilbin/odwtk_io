// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library bytebuf;

//TODO consolidate this with DcmDateTime
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:core/core.dart';
import 'package:utilities/utilities.dart';

//ToDo  1) All public methods should check their args for end of buffer
//      2) Add assertions where appropriate

/**
 * TODO document
 */
class ByteBuf {
  ByteData   _bd;        // The underlying ByteData
  Uint8List  _uint8List; // Uint8List view of the ByteData
  int        _base;      // index of 1st byte in [this] ByteBuf
  int        _rdIdx;     // index of 1st readable byte
  int        _wrIdx;     // index of 1st writable byte
  int        _capacity;  // index of last writable byte + 1
  Endianness _endianness = Endianness.LITTLE_ENDIAN;
  //TODO After performace analysis
  //     1) determine if we should preallocate the list size
  //     2) create a cache of ByteBufs used by the factories if needed

  /// Constructors
  ByteBuf(this._bd, this._rdIdx, this._wrIdx,
          [this._endianness = Endianness.LITTLE_ENDIAN]) {
    _base      = _bd.offsetInBytes;
    _capacity  = _bd.lengthInBytes - _bd.offsetInBytes;
    //_bd        = new ByteData.view(_bd.buffer, _base, _capacity);
    _uint8List = new Uint8List.view(_bd.buffer, _base, _capacity);
  }

  factory ByteBuf.fromUint8List(Uint8List bytes) {
    ByteData bd = new ByteData.view(bytes.buffer);
    return new ByteBuf(bd, 0, bytes.lengthInBytes);
  }

  factory ByteBuf.fromByteData(ByteData bytes) {
    ByteData bd = new ByteData.view(bytes.buffer);
    return new ByteBuf(bd, 0, bytes.lengthInBytes);
  }

  factory ByteBuf.empty(int length) {
    Uint8List uint8 = new Uint8List(length);
    ByteData bd = new ByteData.view(uint8.buffer);
    return new ByteBuf(bd, 0, 0);
  }

  /**
   * Returns a new [ByteBuf] that is a slice of [this]
   */
  ByteBuf slice(int offset, int length) =>
      new ByteBuf(_bd, _chkRdIdx(offset), _checkLengthInBytes(length));

  //TODO is this used/needed?
  void _chkView(ByteData bd, int start, int length) {
    if ((start < _rdIdx) || ((_rdIdx + length) >= _wrIdx)) {
      _createError(start, length);
    }
  }

  /// General getters, setters, etc.
  //TODO verify how these are defined in ByteBuf.java
  ByteData get byteData => _bd;
  Uint8List get uint8List => _uint8List;
  int  get base => _bd.offsetInBytes;
  int  get capacity => _bd.lengthInBytes - _bd.lengthInBytes;
  int  get readableBytes => (_wrIdx - _rdIdx);
  int  get writableBytes => (_capacity - _wrIdx);
  bool get isEmpty => (_rdIdx >= _wrIdx);
  bool get isNotEmpty => (_rdIdx < _wrIdx);
  int  get readIndex => _rdIdx;
  void set readIndex(int i) { _rdIdx = _chkRdIdx(i); }
  int  get writeIndex => _wrIdx;
  void set writeIndex(int i) { _wrIdx = _chkWrIdx(i); }

  /// Returns the ith byte, if in readableBytes, but does not advance the _rdIdx.
  int operator [](int i) => _uint8List[_chkRdIdx(i)];

  void operator []= (int _wrIdx, int value) {
    _uint8List[_chkWrIdx(_wrIdx)] = _inUint8Range(value);
  }

  bool isReadable(int length) => readableBytes >= length;
  bool isWritable(int length) => writableBytes >= length;

  bool notAtReadLimit(int limit) => _rdIdx < limit;
  int _nToLimit(int limit) => limit - _rdIdx;
  int _minToLimit(int limit, int max) => min(_nToLimit(limit), max);

  void set byteData(ByteData bd) {
    _bd = bd;
    _base = _bd.offsetInBytes;
    _capacity = _bd.lengthInBytes;
    _uint8List = new Uint8List.view(_bd.buffer, _base, _capacity);
  }

  int _inUint8Range(int value) {
    return ((value < 0) || (value >= 256)) ? rangeError(
        'Value:$value out of range 0..256.') : value;
  }

  /// Utilities
  /**
   * Sets the readIdx and writeIdx = 0.
   */
  void clearIndices() {
    _rdIdx = 0;
    _wrIdx = 0;
  }

  /**
   * Sets the [readIdx] and [writeIdx] at the same time.
   */
  void setIndices(int readIdx, int writeIdx) {
    _rdIdx = _chkRdIdx(readIdx);
    _wrIdx = _chkWrIdx(writeIdx);
  }

  // Internal validators
  /**
   * Validator for [index] arguments.
   *
   * If the [index] is valid, returns that value; otherwise, throws [RangeError].
   */
  int _chkRdIdx(int index) {
    return ((index < _rdIdx) || (index >= _wrIdx))
            ? rangeError('Index:$index out of range $_rdIdx..$_wrIdx.')
            : index;
  }

  /**
   * Checks that the [ByteBuf] has enough bytes remaining to read the requested [length]
   * elements and returns the [lengthInBytes] to read.
   */
  int checkLength(int length, {int elementSize: 1})
      => _checkLengthInBytes(length * elementSize);

  //TODO need better name
  int _checkLengthInBytes(int bytes) {
    return ((_rdIdx + bytes) <= _wrIdx) ? bytes : _lengthError();
  }

  void _lengthError() {
    parseError("requested [length] exceeds current ByteBuf size");
  }

  /**
   * Returns the [index] that is the limit for a read.
   */
  int readLimit(int length, {int elementSize: 1}) => _rdLimit(length * elementSize);

  bool atLimit(int limit) => (_rdIdx < limit) ? false : true;

  // Returns the [index] that is the limit for the read.
  int _rdLimit(int bytes) {
    //TODO should we support null as meaning the rest of the buffer?
    int limit = _rdIdx + bytes;
    return (limit <= _wrIdx) ? limit
              : parseError('requested [length] exceeds ByteBuf size.');
  }

  /// Peek or Read Bytes

  // Return the current character, but do not advance the index
  int peek() {
    _checkLengthInBytes(1);
    return _peek();
  }

  // USE WITH CARE - fast but no error checking
  int _peek() => _uint8List[_rdIdx];

  int readByte() {
    _checkLengthInBytes(1);
    return _readByte();
  }

  // USE WITH CARE - fast but no error checking
  int _readByte() {
    int char = _uint8List[_rdIdx];
    _rdIdx++;
    return char;
  }

  Uint8List readBytes(int nBytes) {
    Uint8List bytes = new Uint8List(nBytes);
    for(var i = 0; i < nBytes; i++) bytes[i] = uint8List[_rdIdx + i];
    _rdIdx += nBytes;
    return bytes;
  }

  //TODO implement getChar, readChar, setChar writeChar that read UTF8 into UTF16 and
  //     write UTF16 into UTF8.


  /**
   * Returns the unevaluated Value Field (lazy eval)
   */
  //TODO Used?  Needed?
  Uint8List readView(int nBytes) {
     _rdLimit(nBytes);
    Uint8List val = new Uint8List.view(_uint8List.buffer, _rdIdx, nBytes);
    _rdIdx += nBytes;
    return val;
  }

  // Skip over [n] bytes
  int skip(int n) => _rdIdx += _checkLengthInBytes(n);

  int skipSpaces(int length) => skipChars($SPACE, length);

  // Skips up to [length] characters == [char].
  int skipChars(int char, int length) {
    int limit = _rdLimit(length);
    int i = 0;
    for ( _rdIdx; _rdIdx < limit; _rdIdx++) {
      if (_peek() != char) break;
      i++;
    }
    return i;
  }

  int getUint16() => _bd.getUint16(_rdIdx, _endianness);

  /// String Readers
  //TODO add readUtf8String -> UTF16String
  /**
    * Read an Ascii [String] of a maximum of [maxLength] characters.  The
    * [delimiter] indicates the end of the string.
    */
  String readString({int maxLength, int delimiter: $BACKSLASH}) {
     // return _readString(_checkLengthInBytes(maxLength), _inUint8Range(delimiter));
    //print("readString");
    return _readString(maxLength, delimiter);
  }

  // Internal string reader - doesn't check length
  String _readString(int maxLength, int delimiter) {
    //print("_readString");
    //print("maxLength= $maxLength, delimiter=$delimiter");
    //TODO cleanup and get rid of the breaks
    int start = _rdIdx;
    int limit = _rdLimit(maxLength);
    //print('_rs1: _rdIdx=$_rdIdx, start=$start, limit=$limit, delim= $delimiter');
    if (delimiter >= 0) {
      //print('in delim');
      for(_rdIdx; _rdIdx < limit; _rdIdx++) {
        int char = _uint8List[_rdIdx];
        //print('_rs2: _rdIdx=$_rdIdx,limit=$limit, char=${char}');
        //TODO do we need this check for null?
        //if (char != delimiter && char != null) {
        if (char != delimiter) {
          //print('no delim');
          continue;
        } else {
          if (_rdIdx == start) {
            start++;
            limit = _rdLimit(maxLength + 1);
            continue;
          }
          //print('delim');
          limit = _rdIdx;
          //Skip delimiter
          _rdIdx++;
          //print('_rs3: limit=$limit, _rdIdx=$_rdIdx');
          break;
        }
      }
    }
    //TODO figure out how to avoid creating this view
    Uint8List bytes = new Uint8List.view(_uint8List.buffer, start, limit - start);
    //print('_rs4: start=$start, limit=$limit, rdIdx=$_rdIdx, ${ASCII.decode(bytes)}');
    return ASCII.decode(bytes);
  }

  /**
    * Read a [List] of [String]s separated by [delimiter]
    */
  List<String> readStringList(int length, {int maxItemLength: -1,
                                           int delimiter: $BACKSLASH}) {
    //print('readStringList');
    List<String> list = new List();
    int limit = _rdLimit(length);
    //print('rsl1: _rdIdx= $_rdIdx, limit=$limit');
    while (_rdIdx < limit) {
      maxItemLength = min(limit-_rdIdx, maxItemLength);
      //print('rsl2: _rdIdx=$_rdIdx, limit=$limit');
      String s = _readString(maxItemLength, delimiter);
      //print('rsl3: $s');
      list.add(s);
    }
    return list;
  }

  /**
    * Read a [List] of [int]s from [this]
    */
  List<int> readIntegerList(int maxLength,
                            {int maxItemLength: 12,
                             int delimiter: $BACKSLASH}) {
    List list = new List();
    int limit = _rdLimit(maxLength);
    while (_rdIdx < limit) {
      //TODO next line should be readInteger(_rdLength(maxItemLength))
      list.add(readInteger(_minToLimit(limit, maxItemLength)));
      //TODO verify that this generates an error if no backslash is found
      skipChars(delimiter, 1);
    }
    return list;
  }

  // Read a positive integer Iradix 10) of exact length nChars.
  int readPosInt(int nChars, int min, int max) {
    int limit = _rdLimit(nChars);
    int n = 0;
    for(int i = 0; i < nChars; i++) {
      var char = _peek();
      if ((char >= $DIGIT_0) && (char <= $DIGIT_9)) {
        n = (n * 10) + (char - $DIGIT_0);
        _rdIdx++;
      } else parseError('invalid CodeUnit= $char');
    }
    if ((n < min) || (n > max)) parseError('value out of range');
    return n;
  }

  /**
    * Read an integer from [this] ByteBuf.
    *
    * Note: The integer might have leading or training spaces, which are ignored.
    *       When writing the numbers as code units no leading or trailing
    *       whitespace (SPACE only) should be written.
    */
  int readInteger(int maxLength) {
    int limit = _rdLimit(maxLength);
    int n = skipSpaces(_checkLengthInBytes(maxLength));
    n = _readIntegerPart(limit);
    if (_rdIdx != limit) skipSpaces(_nToLimit(limit));
    // we shouldn't have to skip nulls
    return n;
  }

  /**
    *  Check for leading '+' or '-' and return +1 or -1.
    */
  int _readSign() {
    int char = peek();
    if (char == $MINUS) {
      _rdIdx++;
      return -1;
    } else if (char == $PLUS) {
      _rdIdx++;
      return 1;
    } else return 1;
  }

  /**
    * Read the integer part of a number (integer or  decimal) number from a Uint8List.
    *
    * Note: The number (integer or decimal) might have leading or training
    *       spaces.  When writing the numbers as code units no leading or
    *       trailing whitespace should be written.
    */
  int _readIntegerPart(int limit) {
    int value = 0;
    int sign = _readSign();
    while (_rdIdx < limit) {
      var char = _peek();
      if ((char >= $DIGIT_0) && (char <= $DIGIT_9)) {
        value = (value * 10) + (char - $DIGIT_0);
        _rdIdx++;
      } else break;
    }
    return sign * value;
  }

  /**
    * Read the fractional part of a decimal number
    *
    * The fractional part MUST start with a _PERIOD (decimal point).
    */
  num readFractionalPart(int limit) {
    num base = 1;
    num value = 0.0;
    if (peek() != $PERIOD) return 0;
    _rdIdx++;
    while (_rdIdx < limit) {
      int char = _peek();
      if ((char >= $DIGIT_0) && (char <= $DIGIT_9)) {
        base *= 10;
        value = (value + ((char - $DIGIT_0) / base));
        _rdIdx++;
      } else {
        //TODO do we need to check for an error here?
        break;
      }
    }
    return value;
  }

  int _readExponentPart(int limit) {
    int value = 1;
    //TODO could next line use _peek()
    int char = peek();
    if ((char == $E) || (char == $e)) {
      _rdIdx++;
      value = _readIntegerPart(limit);
    } else {
      //There is no exponent
      return 1;
    }
    return pow(10, value);
  }

  /**
    * Reads the bytes as characters and reads a decimal number starting at
    * _rdIdx.  The number has the format [sign]int[.int][Eint].  [maxLength]
    * specifies the maximum length of the decimal string. This method should not
    * read more than [maxLength] characters.
    */
  //TODO make sure that length is not exceeded
  num readDecimal([int maxLength = 16]) {
    int limit = _rdLimit(maxLength);
    skipSpaces(_checkLengthInBytes(maxLength));
    int i = _readIntegerPart(limit);
    num f = 0;
    num e = 1;
    if (_rdIdx < limit) f = readFractionalPart(limit);
    if (_rdIdx < limit) e = _readExponentPart(limit);
    skipSpaces(_nToLimit(limit));
    //TODO Should not need to skip null
    if (i < 0) {
      f = (i - f);
    } else {
      f = (i + f);
    }
    return f * e;
  }

  List<num> readDecimalList(int length,
                            {int delimiter: $BACKSLASH,
                              //TODO what should the default be on the next line
                             int maxItemLength: 16}) {
    List<num> list = new List();
    int limit = _rdLimit(length);
    while (_rdIdx < limit) {
      //Note: the maxLength of VR.DS (decimal string) is 16
      list.add(readDecimal(_minToLimit(limit, 16)));
      //TODO verify that this generates an error if no backslash
      skipChars($BACKSLASH, 1);
    }
    return list;
  }

  /**
    * Read a Universally Unique Identifier (UUID)
    * See http://docs.oracle.com/javase/7/docs/api/java/util/UUID.html
    */
  //TODO this should check for backslash or other delimiter
  List<Uuid> readUuidList({maxLength: 16, int delimiter: $BACKSLASH}) {
    List list = new List();
    for (var i = 0; i < maxLength; i++) {
      list.add(readUuid());
    }
    return list;
  }

  Uint8List readUuid() => Uuid.random(buffer: readBytes(16));

  /**
    * Read a OSI Object Identification UID
    * Based on the OSI Object Identification (numeric form) as defined by
    * the ISO 8824 standard.  See DICOM PS3.5 Section 9 (page 61 in .pdf)
    */
  //TODO should this be returning a UID object rather than a string?
  String readUid({maxLength: 64}) {
    _rdLimit(maxLength);
    String uidStr = readString(maxLength: maxLength);
    //TODO make this next line work
    //UidUtils.isValid(uidStr);
    return uidStr;
  }

  //TODO finish and test
  List<String> readUidList(length, {int maxItemLength: 64}) {
    int limit = _rdLimit(length);
    List list = new List();
    for (var i = _rdIdx; i < limit; i++) {
      list.add(readUid(maxLength: _minToLimit(limit, maxItemLength)));
    }
    return list;
  }

  //TODO this should be checking the length
  Uri readUri(int maxLength) => Uri.parse(readString(maxLength: maxLength));

  /// Binary Readers
  //TODO Add xx.BYTES_PER_ELEMENT or xx.elementSizeInBytes
  /// Int8, Int8List, Uint8, and Uint8List
  int readInt8() {
    _checkLengthInBytes(1);
    var val = _bd.getInt8(_rdIdx);
    _rdIdx++;
    return val;
  }

  Int8List readInt8List(int bytes) {
    int nBytes = _checkLengthInBytes(bytes);
    Int8List list = new Int8List(nBytes);
    for (var i = 0; i < nBytes; i++) list[i] = _bd.getInt8(_rdIdx + i);
    _rdIdx += nBytes;
    return list;
  }

  int readUint8() {
    _checkLengthInBytes(1);
    var val = _bd.getUint8(_rdIdx);
    _rdIdx++;
    return val;
  }

  Uint8List readUint8List(int bytes) {
    int nBytes = _checkLengthInBytes(bytes);
    Uint8List list = new Uint8List(nBytes);
    for (var i = 0; i < nBytes; i++) list[i] = _bd.getUint8(_rdIdx + i);
    _rdIdx += nBytes;
    return list;
  }

  int checkSize(int length, int sizeInBytes) => _chkSize(length, sizeInBytes);

  int _chkSize(int nBytes, int sizeInBytes) {
    if ((nBytes % sizeInBytes) != 0)
      parseError('vfLength=$nBytes is not a multiple of $sizeInBytes');
    return _checkLengthInBytes(nBytes) ~/ sizeInBytes;
  }

  /// Int16, Int16List, Uint16, and Uint16List
  static const int _int16Size = 2;  // Size in bytes
  //** update all readers to look like this
  int readInt16() {
    _checkLengthInBytes(_int16Size);
    int value = _bd.getInt16(_rdIdx, _endianness);
    _rdIdx += _int16Size;
    return value;
  }

  //TODO Should the list readers take length or lengthInBytes?

  Int16List readInt16List(int vfLength) {
    int length = _chkSize(vfLength, _int16Size);
    List list = new Int16List(length);
    for (int i = 0; i < length; i++) {
      list[i] = _bd.getInt16(_rdIdx, _endianness);
      _rdIdx += _int16Size;
    }
    return list;
  }

  int peekUint16() {
    _checkLengthInBytes(_int16Size);
    int value = _bd.getUint16(_rdIdx, _endianness);
    return value;
  }

  int readUint16() {
    int value = peekUint16();
    _rdIdx += _int16Size;
    return value;
  }

  Uint16List readUint16List(int vfLength) {
    int length = _chkSize(vfLength, _int16Size);
    Uint16List list = new Uint16List(length);
    for (int i = 0; i < length; i++) {
      list[i] = _bd.getUint16(_rdIdx, _endianness);
      _rdIdx += _int16Size;
    }
    return list;
  }

  /// Int32, Uint32List, Uint32, and Uint32List
  static const int _int32Size = 4; //Size in bytes

  int readInt32() {
    _checkLengthInBytes(_int32Size);
    int value = _bd.getInt32(_rdIdx, _endianness);
    _rdIdx += _int32Size;
    return value;
  }

  Int32List readInt32List(int vfLength) {
    int length = _chkSize(vfLength, _int32Size);
    Int32List list = new Int32List(length);
    for (int i = 0; i < length; i++) {
      list[i] = _bd.getInt32(_rdIdx, _endianness);
      _rdIdx += _int32Size;
    }
    return list;
  }

  int readUint32() {
    _checkLengthInBytes(_int32Size);
    int value = _bd.getUint32(_rdIdx, _endianness);
    _rdIdx += _int32Size;
    return value;
  }

  Uint32List readUint32List(int vfLength) {
    int length = _chkSize(vfLength, _int32Size);
    Uint32List list = new Uint32List(length);
    for (int i = 0; i < length; i++) {
      list[i] = _bd.getUint32(_rdIdx, _endianness);
      _rdIdx += _int32Size;
    }
    return list;
  }

  /// Int64, Int64List, Uint64, and Uint64List
  static const int _int64Size = 8;  //Size bytes

  int readInt64() {
    _checkLengthInBytes(_int64Size);
    int value = _bd.getInt64(_rdIdx, _endianness);
    _rdIdx += _int64Size;
    return value;
  }

  Int64List readInt64List(int vfLength) {
    int length = _chkSize(vfLength, _int64Size);
    Int64List list = new Int64List(length);
    for (int i = 0; i < length; i++) {
      list[i] = _bd.getInt64(_rdIdx, _endianness);
      _rdIdx += _int64Size;
    }
    return list;
  }

  int readUint64() {
    _checkLengthInBytes(_int64Size);
    int value = _bd.getUint64(_rdIdx, _endianness);
    _rdIdx += _int64Size;
    return value;
  }

  Uint64List readUint64List(int vfLength) {
    int length = _chkSize(vfLength, _int64Size);
    Uint64List list = new Uint64List(length);
    for (int i = 0; i < length; i++) {
      list[i] = _bd.getUint64(_rdIdx, _endianness);
      _rdIdx += _int64Size;
    }
    return list;
  }

  /// [Float32] single values and lists
  static const _float32Size = 4;  //Size in bytes

  double readFloat32() {
    _checkLengthInBytes(_float32Size);
    double value = _bd.getFloat32(_rdIdx, _endianness);
    _rdIdx += _float32Size;
    return value;
  }

  Float32List readFloat32List(int vfLength) {
    int length = _chkSize(vfLength, _float32Size);
    Float32List list = new Float32List(length);
    for (int i = 0; i < length; i++) {
      list[i] = _bd.getFloat32(_rdIdx, _endianness);
      _rdIdx += _float32Size;
    }
    return list;
  }

  /// Float64 and Float64List
  static const int _float64Size = 8;  //Size in bytes

  double readFloat64() {
   _checkLengthInBytes(_float64Size);
    double value = _bd.getFloat64(_rdIdx, _endianness);
    _rdIdx += _float64Size;
    return value;
  }

  Float64List readFloat64List(int vfLength) {
    int length = _chkSize(vfLength, _float64Size);
    Float64List list = new Float64List(length);
    for (int i = 0; i < length; i++) {
      list[i] = _bd.getFloat64(_rdIdx, _endianness);
      _rdIdx += _float64Size;
    }
    return list;
  }


  /// Writer Utilities
  /// Methods for managing public write methods

  int _chkWrIdx(int index) {
    return ((index < _wrIdx) || (index >= _capacity)) ? rangeError(
        'Write index out of range $_wrIdx..$_capacity.') : index;
  }

  int _chkWrLen(int length, {int elementSize: 1, int maxLength: -1}) {
    int len = length * elementSize;
    return ((len > writableBytes) || (len > maxLength))
              ? rangeError('Write length exceeds remaining=$writableBytes')
              : length;
  }

  /// String Writers
  //TODO look at how this is done in Convert
  void writeString(String s, {int maxLength: -1}) {
    int limit = _chkWrLen(s.length, elementSize: 1, maxLength: maxLength);
    for(var i = 0; i < limit; i++) {
      write(s.codeUnitAt(i));
    }
  }

  void writeStringList(List<String> l, {int maxItemLength: -1}) {
    for(var i = 0; i < l.length; i++)
      writeString(l[i], maxLength: maxItemLength);
  }

  /// Binary Writers
  //TODO do we need the _inRange check?  will setInt8 signal the error?
  void writeInt8(int i) {
    _bd.setInt8(_wrIdx, _chkWrLen(i, elementSize: 1));
    _wrIdx++;

  }

  void writeInt8List(Int8List l) {
    int limit = _chkWrLen(l.length, elementSize: 1);
    for(var i = 0; i < limit; i++) writeInt8(i);
  }

  void writeUint8(int i) {
    _bd.setInt8(_wrIdx, _chkWrLen(i));
    _wrIdx++;
  }
  void write(int i) => writeUint8(i);

  void writeUint8List(Uint8List l) {
    int limit = _chkWrLen(l.length, elementSize: 1);
    for(var i = 0; i < l.length; i++) writeUint8(i);
  }

  void writeInt16(int i) {
    _bd.setInt16(_wrIdx, i, _endianness);
    _wrIdx += 2;
  }

  void writeInt16List(Uint8List l) {
    int limit = _chkWrLen(l.length, elementSize: 2);
    for(var i = 0; i < l.length; i++) writeInt16(i);
  }

  void writeUint16(int i) {
    _bd.setUint16(_wrIdx, i, _endianness);
    _wrIdx += 2;
  }

  void writeUint16List(Uint8List l) {
    int limit = _chkWrLen(l.length, elementSize: 2);
    for(var i = 0; i < l.length; i++) writeInt16(i);
  }

  void writeInt32(int i) {
    _bd.setInt32(_wrIdx, i, _endianness);
    _wrIdx += 4;
  }

  void writeUint32(int i) {
    _bd.setUint32(_wrIdx, i, _endianness);
    _wrIdx += 4;
  }

  void writeInt64(int i) {
    _bd.setInt64(_wrIdx, i, _endianness);
    _wrIdx += 8;
  }

  void writeUint64(int i) {
    _bd.setUint64(_wrIdx, i, _endianness);
    _wrIdx += 8;
  }

  void writeFloat32(double n) {
    _bd.setFloat32(_wrIdx, n, _endianness);
    _wrIdx += 4;
  }

  void writeFloat64(double n) {
    _bd.setFloat64(_wrIdx, n, _endianness);
    _wrIdx += 8;
  }

  /// Errors

  void charsetError(int char) {
    String s = ASCII.decode([char]);
    throw new Exception('ValueField has invalid character = $char= $s');
  }

  // Constructor error
  void _createError(int start, int end) {
    var msg = "Invalid arguments to ByteBuf: $this, $start..$end";
    throw new ArgumentError(msg);
  }

  String get debug =>
      "BB: rdIdx=$_rdIdx, wrIdx=$_wrIdx, readableBytes=$readableBytes";

  //TODO finish
  //int hashCode() {

  //}

  String toString() =>
      "ByteBuf($hashCode: BD=$_bd R=$_rdIdx, W=$_wrIdx):'${readString(maxLength:15, delimiter: -1)}'";
}
