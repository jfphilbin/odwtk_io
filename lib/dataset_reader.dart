// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library dataset_reader;

//import 'dart:convert';
import 'dart:typed_data';

import 'package:dictionary/vr.dart';
import 'package:dictionary/tag.dart';
import 'package:core/core.dart';
import 'package:io/typed_data_reader.dart';
import 'package:utilities/utilities.dart';

/**
 * Returns a Dataset read from a [buffer].
 */
class DatasetReader extends TypedDataReader {
  Dataset ds;  // Holds the dataset being read

  DatasetReader(Uint8List bytes, [int start, int length])
      : super.fromUint8List(bytes, start, length);

  DatasetReader.fmi(Uint8List bytes) : super.fromUint8List(bytes) {
    ds = new Dataset(DSType.FILE_META_INFO, null);
  }




  //***** DICOM Attribute Utilities *****

  /*
  /// Peek at next tag - doesn't move the [TypedDataReader.index]
  int peekTag() {
    int group = peek16();
    int element = peek16();
    print('grp=$group, elt=$element');
    return (group << 16) + element;
  }
  */
  /// Read the DICOM Attribute Tag
  int readTag() {
    int group   = readUint16();
    int element = readUint16();
    return (group << 16) + element;
  }


  /// Read a Value Representation (VR).  See PS3.10.
  VR readVR() {
    int c1 = uInt8();
    int c2 = uInt8();
    int vrCode = c2 + (c1 << 8);
    //print(intToHex(vrCode));
    return VR.codeToVR(vrCode);
  }

  /// Read the Value Field Length
  /// Note: This is always an even number, but the last byte might be
  /// padding.  See DICOM PS3.5.
  //TODO fix documentation above
  int readVFLength(bool isLarge) {
    if (isLarge) {
      skip(2);
      return readUint32();
    } else {
      return int16();
    }
  }

  int getVFLimit(int vfLength) {
    int foo = index + vfLength;
    print('index=$index, vrLength=$vfLength');
    return foo;
  }

  // Utility for debugging
  void printAttribute(tag, vr, value) {
    print('[${tagToHex(tag)}, $vr, $value]');
  }

  Attribute readXvrAttribute() {
    // reader.debug(0, 16);
    int tag = readTag();
    print('tag=${tagToDcmFmt(tag)}');
    VR  vr = readVR();
    print('$vr ${vr.name}');
    var values;
    int vfLength = readVFLength(vr.isLarge);
    print('vfLength=$vfLength');
    int currentIndex = index;
    if (vr == VR.SQ) {
      values = readSequence(tag, vr, vfLength);
    } else {
      int vfLimit = getVFLimit(vfLength);
      print('vfLength=$vfLength, vfLimit=$vfLimit');
      values = readSimpleValue(tag, vr, vfLength);
    }
    var a = new Attribute(tag, vr, vfLength, values);
    if (a != null) ds.add(a);
    print(a);
    return a;
  }

  Attribute readIvrAttribute() {
    // reader.debug(0, 16);
    int tag = readTag();
    print('tag=${tagToDcmFmt(tag)}');
    //TODO next line
    VR vr = null;
    //VR vr = tagToVRMap(tag);
    //print('$vr ${vr.name}');
    var values;
    int vfLength = readVFLength(true);
    print('vfLength=$vfLength');
    int currentIndex = index;
    //if (vr == VR.SQ) {
    //  values = readSequence(vfLength);
    //} else {
      int vfLimit = getVFLimit(vfLength);
      print('vfLength=$vfLength, vfLimit=$vfLimit');
      values = readSimpleValue(tag, vr, vfLength);
    //}
    var attr = new Attribute(tag, vr, vfLength, values);
    print(attr.toString());
    return attr;
  }

  Attribute readFmiAttribute() {
    int group = peekUint16();
    return (group == 2) ? readXvrAttribute() : null;
  }

  /// Read a top level [Dataset]
  Dataset readTopLevel() {
    ds = new Dataset.top();
    return readDataset(limit);
  }

  /// Read attributes from the [TypedDataReader] into the [Dataset] [ds].
  /// Any leading [Item] header has already been read.
  Dataset readDataset(int vfLimit) {
    int tag = readTag();
    VR vr = readVR();
    int vfLength = readVFLength(vr.isLarge);
    int vfLimit = getVFLimit(vfLength);
    while (isNotEmpty) {
      if (vr == VR.SQ) {
        if (vfLength > -1) {
          ds.add(tag, vr, readSequence(tag, vr, vfLimit));
        } else if (vfLength == -1) {
          ds.add(tag, vr, readDelimitedSequence(tag, vr));
        } else {
          error('invalid vfLength = $vfLength');
        }
      } else ds.add(tag, vr, readSimpleValue(tag, vr, vfLimit));
    }
    return ds;
  }

  /**
     * Read Sequence
     *
     * TODO document
     */
  Sequence readSequence(int tag, VR vr, int vfLength) {
    if (vr != VR.SQ) error('Callded readSequence with tag=$tag');
    int vfLimit = getVFLimit(vfLength);
    List items = new List();
    while (index < vfLimit) items.add(readItem());
    return new Sequence(tag, vr, ds, items);
  }

  Sequence readDelimitedSequence(int tag, VR vr) {
    //TODO Is this the best way find the delimiter?
    int limit = findSequenceDelimiter();
    Sequence seq = readSequence(tag, vr, limit - index);
    int endTag = readTag();
    if (endTag != $EndOfSequence) parseError('in Delimited Sequence');
    int vfLength = readUint32();
    if (vfLength != 0) parseError('Sequence END length field not zero');
    return seq;
  }

  /// Read the [$StartItemTag] and [length] and return [length].
  int readItemLength() {
    int tag = readTag();
    if (tag != $StartOfItem) parseError("wrong tag in readItem");
    int vfLength = readUint32();
    return vfLength;
  }
  /// Read the dataset contained in an [Item] into the [Dataset] [ds].  Returns the
  /// lengthInBytes of the [Dataset] read.
  int readItem() {
    int vfLength = readItemLength();
    Dataset dsParent = ds;
    ds = new Dataset(DSType.ITEM, dsParent);
    if (vfLength > -1) {
      readDataset(getVFLimit(vfLength));
    } else if (vfLength == -1) {
      readDelimitedItem();
    } else {
      error('Invalid Item Length=$vfLength');
    }
    ds = dsParent;
    return vfLength;
  }

  /**
   * Reads a delimited [Item] and returns the [Dataset] contained in that item.
   *
   * The [Item] is read by 1) Saving the current [Dataset], creating a new [Dataset],
   * and then read the attributes contained in the [Item] into the new [Dataset].  After
   * the dataset is read, the previous dataset [dsParent] is made [ds].
   */
  int readDelimitedItem() {
    int limit = findItemDelimiter();
    Dataset dsParent = ds;
    Dataset dsNew = new Dataset(DSType.ITEM, dsParent);
    ds = dsNew;
    dsNew = readDataset(limit);
    int endTag = readTag();
    if (endTag != $EndOfItems) parseError('in Delimited Item');
    int vfLength = readUint32();
    if (vfLength != 0) parseError('Item END length field not zero');
    ds = dsParent;
    return limit;
  }

  //TODO create a Union type instead of Object
  readSimpleValue(tag, vr, vfLength) {
    //if (evalMode == EvalMode.LAZY) return readBytes(vfLength);
    //This table is  ordered by tag frequency with most frequent first.
    //TODO some of these fields are returning strings instead of parsed values,
    //     e.g. TM, DA, DT,...
    switch (vr) {
      // The VRs in this list are in order of frequency of occurance - most to least.
      //TODO Any of these methods that might have a padding char have to change their lengths.
      case VR.DS:  // Decimal String
        return readDecimalList(vfLength, maxItemLength: vr.maxLength);
      case VR.TM:  // Time
        //TODO readTime(vfLength);
        return readStringList(vfLength, maxItemLength: vr.maxLength);
      case VR.IS:  // Integer String
        return readIntegerList(vfLength, maxItemLength: vr.maxLength);
      case VR.UI:  // UID
        return readUidList(vfLength, maxItemLength: vr.maxLength);
      case VR.BR:  // Bulkdata Reference
        return readBDReference(vr, vfLength);
      case VR.US:  // Unsigned Short
        return uInt16List(vfLength);
      case VR.FD:  // Floating Point Double
        return float64List(vfLength);
      case VR.CS:  // Code String
        return readStringList(vfLength, maxItemLength: vr.maxLength);
      case VR.DT:  // DateTime
        //TODO readDateTime(vfLength)
        return readStringList(vfLength, maxItemLength: vr.maxLength);
      case VR.SS:  // Signed Short
        return int16List(vfLength);
      // VR.SQ should be here by frequency, but it is handled in readSequence.
      case VR.LT:  // Long Text
        return readString(vfLength);
      //TODO should this be here?
      //case VR.UN:  // Unknown
      //  return readOtherBytes(vfLength);
      case VR.LO:  // Long String - maxLength = 64
        return readStringList(vfLength, maxItemLength: vr.maxLength);
      case VR.SH:  // Short String - maxLength = 16
        return (tag == $TimezoneOffsetFromUTC)
                 // Special handling for TimezoneOffsetFromUTC tag.
                 ? readTimeZoneOffset()
                 : readStringList(vfLength, maxItemLength: vr.maxLength);
      case VR.UL:  // Unsigned Long
        return readUint32();
      case VR.DA:  // Date
        return readStringList(vfLength, maxItemLength: vr.maxLength);
      case VR.PN:  // Person Name
        return readPersonNameList(vfLength, maxItemLength: vr.maxLength);
      case VR.ST:  // Short Text - maxLength = 1024
        return readString(vfLength);
      case VR.SL:  // Signed Long
        return int32List(vfLength);
      case VR.FL:  // Floating Point Single
        return float32List(vfLength);
      case VR.UR:  // URI/URL
        return readUri(vfLength);
      case VR.AE:  // AE Title - maxLength = 16
        return readStringList(vfLength, maxItemLength: vr.maxLength);
      // probably no attributes of the types below
      case VR.AS:  // Age String - Length must be 4
        return readAgeString(vfLength);
      case VR.AT:  // Attribute Tag
        return readTagList(vfLength);
      case VR.UT:  // Unlimited Text - maxLength = (2**32)-2
        return readString(vfLength);
      case VR.OB:  // Other Byte
        return readOtherBytes(vfLength);
      case VR.OW:  // Other Word
        return readOtherWords(vfLength);
      case VR.OF:  // Other Float
        return readOtherFloats(vfLength);
      case VR.OD:  // Other Double
        return readOtherDoubles(vfLength);
      case VR.UN:  // Unknown
        return readOtherBytes(vfLength);
      case VR.SQ:
        //TODO should probably throw
        return parseError('vr=SQ in readSimpleAttribute');
      default:
        parseError("in readValueField");
    }
  }

  //Person Names are currently unparsed strings
  List<String> readPersonNameList(int vfLength, {maxItemLength: 64}) {
    List list = new List();
    while (index < vfLength) {
      list.add(readPersonName(maxItemLength));
    }
    return list;
  }

  String readPersonName(int length) {
    return readString(length);
    //TODO do we want to parse it now?
    //return PersonName.parse(bb.readString(length));
  }

  BDReference readBDReference(VR vr, int vfLength) {
    // VR.BD has only one value with four fields
    int offset = readUint32();
    int length = readUint32();
    // The first 3 fields contain 10 bytes
    Uri uri = readUri(vfLength - 10);
    return new BDReference(vr, offset, length, uri);
  }

  Uint32List readTagList(vfLength) {
    int length = checkSize(vfLength, Uint32List.BYTES_PER_ELEMENT);
    Uint32List list = new Uint32List(length);
    for (var i = 0; i < length; i++) list[i] = readTag();
    return list;
  }

  //TODO this should be checking that vfLength
  String readAgeString(vfLength) {
    if (vfLength != 4) parseError('VR=AS age string length != 4');
    return readString(vfLength);
  }
  Uint8List readOtherBytes(int vfLength) {
    if (vfLength == -1) vfLength = findItemDelimiter();
    return uInt8List(vfLength);
  }

  Uint16List readOtherWords(int vfLength) {
    if (vfLength == -1) vfLength = findItemDelimiter();
    return uInt16List(vfLength);
  }

  Float32List readOtherFloats(int vfLength) {
    if (vfLength == -1) vfLength = findItemDelimiter();
    return float32List(vfLength);
  }

  Float64List readOtherDoubles(int vfLength) {
    if (vfLength == -1) vfLength = findItemDelimiter();
    return float64List(vfLength);
  }

  /// DICOM dates have 4 to 8 characters: YYYYMMDD, YYYYMM, or YYYY.
  //TODO Does not check for the correct number of days in month
  //TODO Does not remove trailing spaces
  //TODO The Min and Max years should be defined in System? and should be setable for
  //     the buffer.
  Date readDate(int vfLength, bool parse) {
    if ((vfLength < 4) || (vfLength > 8)) parseError('bad Date');
    int y, m, d;
    if (vfLength >= 4) y = readPositiveInteger(4, 1980, 2020);
    if (vfLength >= 6) m = readPositiveInteger(2, 1, 12);
    if (vfLength == 8) d = readPositiveInteger(2, 1, 32);
    //if (vfLength == 8) d = readPositiveInteger(2, 1, DateTime. maxDaysInMonth(y, m));
    return new Date(y, m, d);
  }

  void _checkRange(int min, int val, int max) {
    if ((min > val) && (val > max))
      parseError('value out of range $min <= $val <= $max');
  }

  _readHour(vfLength) {

  }
  //TODO clean this up -
  /// DICOM Times use a 24 hour clock and are in the format HHMMSS.FFFFFF.  The
  /// maximum field size is 16 bytes.  Trailing spaces are allowed.  Only the [hour]
  /// field is required.
  DateTime readDcmTime(int vfLength) {
    _checkRange(2, vfLength, 16);
    int vfLimit = getVFLimit(vfLength);
    int h = readPositiveInteger(2, 0, 23);
    if (isEmpty) return new DateTime(0, 0, 0, h);
    int m = readPositiveInteger(2, 0, 60);
    if (isEmpty) return new DateTime(0, 0, 0, h, m);
    int s = readPositiveInteger(2, 0, 59);
    if (isEmpty) return new DateTime(0, 0, 0, h, m, s);
    //TODO if needed - if (!atLocalLimit(vfLimit))
    int ms = readFractionalPart(7);
    Duration tzOffset = readTimeZoneOffset();;
    var dateTime = new DateTime(0, 0, 0, h, m, s, ms);
    return dateTime.add(tzOffset);
  }

  //Note: DICOM timezone tag is (0008,0201) TimezoneOffsetFromUTC, VR=SH.
  //      If this tag appears in study all dates and times are in this timezone
  Duration readTimeZoneOffset() {
    //The DICOM Timezone is alway 5 characters long [+,-]hhmm
    int sign, hours, minutes;
    int char = uInt8();
    if (char == $MINUS) {
      sign = -1;
    } else if (char == $PLUS) {
      sign = 1;
    }
    //TODO
    hours = sign * readPositiveInteger(2, 0, 23);
    minutes = sign * readPositiveInteger(2, 0, 59);
    return new Duration(hours: hours, minutes: minutes);
  }

  DateTime readDateTime(int nBytes) {
    int y = readPositiveInteger(4, 1970, System.StartTime.year);
    int m = readPositiveInteger(2, 1, 12);
    int d = readPositiveInteger(2, 1, 31);
    int h = readPositiveInteger(2, 0, 23);
    int min = readPositiveInteger(2, 0, 59);
    int s = readPositiveInteger(2, 0, 60);
    int char = peekChar();
    int f = 0;
    Duration tz;
    if (char == $PERIOD) {
      skip(1);
      int f = readPositiveInteger(7, 0, 9999999);
      char = peekChar();
      if ((char == $PLUS) || (char == $MINUS)) {
        tz = readTimeZoneOffset();
      }
    } else if ((char == $PLUS) || (char == $MINUS)) {
      tz = readTimeZoneOffset();
    }
    DateTime dt = new DateTime(y, m, d, min, s, f);
    return dt.add(tz);
  }

  //***** Sequences and Items of _undefined length_  *****

  // Sequence and Item delimiters
  // Note: the 4 byte vfLength following the Delimiter tags has a value of 0x00000000.
  static const int ITEM_START_TAG = 0xFFFEE000;
  static const int ITEM_END_TAG = 0xFFFE000D; // Item Delimitation Tag
  static const int SEQUENCE_END_TAG = 0xFFFE00DD; // Sequence Delimitation Tag
  static const int DELIMITER_GROUP = 0xFFFF;
  static const int SEQUENCE_DELIMITER_ELEMENT = 0x00DD;
  static const int ITEM_DELIMITER_ELEMENT = 0x000D;

  ///Used to check for Sequence delimiters.
  bool isSequenceEnd() => _isDelimitedEnd(SEQUENCE_DELIMITER_ELEMENT);
  bool isNotSequenceEnd() => isSequenceEnd == false;

  bool isItemEnd() => _isDelimitedEnd(ITEM_DELIMITER_ELEMENT);
  bool isNotItemEnd() => isItemEnd == false;

  bool _isDelimitedEnd(int delimiter) {
    if (peekUint16(0) != DELIMITER_GROUP) return false;
    if (peekUint16(2) != delimiter) return false;
    skip(4);
    //TODO remove the next two lines and make the line above skip(8)
    var len = int32();
    if (len != 0) parseError("in checkForItemDelimiter");
    return true;
  }

  int findSequenceDelimiter() =>
      _findDelimiter(SEQUENCE_DELIMITER_ELEMENT);

  int findItemDelimiter() =>
      _findDelimiter(ITEM_DELIMITER_ELEMENT);

  int _findDelimiter(int delimiter) {
    int i;
    for (var i = index; i < limit; i++) {
      if (peekUint16(0) != DELIMITER_GROUP) continue;
      if (peekUint16(2) != delimiter) continue;
    }
    if (i >= limit) parseError('No delimiter=$delimiter found!');
    return i - index;
  }

  /*
//TODO Move these procedures to DatasetReader
  /**
   * Read the DICOM File Meta Information from [this].
   *
   *    Note: "DICM" = 1296255300 as a 32 bit integer
   */
  //TODO verify the int value for dicm
  //var dicm = new ByteData.view("DICM".codeUnits);
 // final int dicm = 1296255300;

  bool isPart10Object() {
    Uint8List padding = uInt8List(128);
    print(padding);
    for(int i = 0; i < 128; i++) {
      if (padding[i] != 0) return false;
    }
    Uint8List magic = uInt8List(4);
    var s = ASCII.decode(magic);
    print('magic= $s');
    if (magic != 'DICM') return false;
    return true;
  }

  Dataset readFileMetaInfo() {

  }

  Dataset readMintHeader() {

  }
 */
}
