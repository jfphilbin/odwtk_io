// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library file_meta_info;

import 'dart:typed_data';

import 'package:core/core.dart';
import 'package:dictionary/dictionary.dart';
import 'package:dictionary/uid.dart';
import 'package:io/io.dart';

//TODO move to io
/*
 * A class for managing DICOM File Meta Information
 *
 * Default as supplied where needed.
 */
//TODO versions should use Versions, AETitles should have a class
class FileMetaInfo {
  int groupLength; // Length of FMI
  List version; // Sematic Versioning 2.0
  //TODO Next four should be UIDs
  UID mediaStorageSopClass;
  UID mediaStorageSopInstance;
  UID transferSyntax = UID.ImplicitVRLittleEndianDefaultTransferSyntaxforDICOM;
  UID implementationClass; // ???
  String implementationVersion;
  String sourceAETitle;
  String sendingAETitle;
  String receivingAETitle;
  String privateCreator;
  Uint8List privateInformation;

  FileMetaInfo._empty();

  FileMetaInfo({this.groupLength, this.transferSyntax, this.mediaStorageSopClass,
      this.mediaStorageSopInstance, this.implementationClass, this.implementationVersion,
      this.sourceAETitle, this.sendingAETitle, this.receivingAETitle, this.privateCreator,
      this.privateInformation});

  FileMetaInfo.fromDataset(Dataset ds) {
    groupLength = ds[$FileMetaInformationGroupLength].value;
    version = ds[$FileMetaInformationVersion].value;
    mediaStorageSopClass = ds[$MediaStorageSOPClassUID].value[0];
    mediaStorageSopInstance = ds[$MediaStorageSOPInstanceUID].value[0];
    transferSyntax = ds[$TransferSyntaxUID].value[0];
    implementationClass = ds[$ImplementationClassUID].value[0];
    implementationVersion = ds[$ImplementationVersionName].value[0];
    sourceAETitle = ds[$SourceApplicationEntityTitle];
    sendingAETitle = ds[$SendingApplicationEntityTitle];
    receivingAETitle = ds[$ReceivingApplicationEntityTitle];
    privateCreator = ds[$PrivateInformationCreatorUID];
    privateInformation = ds[$PrivateInformation];
  }


  /// Returns true if the [DatasetReader] has the 128 byte Preamble and the
  /// *DICOM Prefix* = "DICM" in ASCII in the next four bytes.  This this is likely
  /// a DICOM object defined in PS 3.10; otherwise, returns false.  If true the
  /// [DatasetReader.index] is incremented by 132.  If false the index remains the
  /// same.
  ///
  ///     Note: "DICM" = [68, 73, 67, 77] as a list of ASCII characters
  ///           "DICM" = 0x4449434d as a 32 bit hex integer
  static bool hasDicomPrefix(DatasetReader dr) {
    const dicomPrefix = 0x4449434d;
    print(dr.index);
    dr.seek(128);
    if (68 != dr.uInt8()) return false;
    if (73 != dr.uInt8()) return false;
    if (67 != dr.uInt8()) return false;
    if (77 != dr.uInt8()) return false;
    return true;
  }

  /// If the [DatasetReader] has a *DICOM Prefix*, then this method will try
  /// to read File Meta Information attributes from the object.  If successful,
  /// returns a [FileMetaInfo] object, with the [DatasetReader.index] just after
  /// the end of the [FileMetaInfo] in the buffer.  If unsuccessful, returns [null]
  /// with the [DatasetReader.index] at the same point as when this method was
  /// invoked.
  static FileMetaInfo maybeRead(Uint8List bytes) {
    DatasetReader dr = new DatasetReader.fmi(bytes);
    SavedIndex savedIndex = dr.saveIndex();
    print(savedIndex);
    if (hasDicomPrefix(dr)) {
      return _read(dr);
    } else {
      dr.restoreIndex(savedIndex);
      return null;
    }
  }

  static FileMetaInfo read(Uint8List bytes) {
    DatasetReader dr = new DatasetReader.fmi(bytes);
    return _read(dr);
  }

  static _read(DatasetReader dr) {
    Attribute a;
    while (dr.readFmiAttribute() != null);
    return new FileMetaInfo.fromDataset(dr.ds);
  }

  String toString() => """
{ GroupLength:             $groupLength,
  TransferSyntax:          $transferSyntax,
  MediaStorageSopClass:    $mediaStorageSopClass,
  MediaStorageSopInstance: $mediaStorageSopInstance,
  ImplementationClass:     $implementationClass,
  ImplementationVersion:   $implementationVersion,
  SourceAETitle:           $sourceAETitle,
  SendingAETitle:          $sendingAETitle,
  ReceivingAETitle:        $receivingAETitle,
  PrivateCreator:          $privateCreator,
  PrivateInformation:      $privateInformation
}
""";

}
