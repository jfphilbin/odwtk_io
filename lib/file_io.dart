// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library fileio;

import 'dart:async';
import 'dart:io';
import 'dart:typed_data';

import 'package:core/dataset.dart';
import 'package:io/dataset_reader.dart';

//TODO
// 1. read a file from String
// 2. read a file from Uri
// 3. Read a file from File
// 4. read a file from Uint8List
  /**
   * Async File access
   */

Future<Dataset> readFileFromUri(Uri uri) => readFileFromPath(uri.path);

Future<Dataset> readFileFromPath(String path) => readFileFromFile(new File(path));

Future<Dataset> readFileFromFile(File file) {
  return file.readAsBytes().then((Uint8List bytes) {
      var dsReader = new DatasetReader(bytes);
      return dsReader.readTopLevel();
  });
}

/**
 * Sync File access
 */
Dataset readFileFromUriSync(Uri uri) => readFileFromPathSync(uri.path);

Dataset readFileFromPathSync(String path) => readFileFromFileSync(new File(path));

Dataset readFileFromFileSync(File file) {
  Uint8List bytes = file.readAsBytesSync();
  var reader = new DatasetReader(bytes);
  return reader.readTopLevel();
}

DatasetReader datasetReaderFromPathSync(String path) =>
    datasetReaderFromFileSync(new File(path));

DatasetReader datasetReaderFromFileSync(File file) {
  var bytes = file.readAsBytesSync();
  return new DatasetReader(bytes);
}


