// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library TypedDataReader;

import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';


import 'package:io/io_error.dart';
import 'package:utilities/ascii.dart';
import 'package:dictionary/uid.dart';
import 'package:utilities/uuid.dart';

class SavedIndex {
  int _index;

  SavedIndex(this._index);
}

/**
 * Read values from a [ByteBuffer].
 *
 * Reads [int]s, [float]s, [ByteData]s ... from a ByteBuffer.  It is designed to be
 * able to read all [TypedData] object.
 */
class TypedDataReader {
  static const _DEFAULT_BUFFER_SIZE = 8192;
  ByteBuffer  _buffer;
  int          _index = 0;
  int          _limit;    // When _index == _limit the buffer is empty.
  Uint8List   _bytes;
  ByteData    _bd;
  Endianness _endian = Endianness.HOST_ENDIAN;  // The default endianness

  /// Constructors
  TypedDataReader([this._buffer, this._index, this._limit, this._endian]) {
    if (_buffer == null) {
      // No need to checkLimit here - _index = 0 and _limit = _DEFAULT_BUFFER_SIZE
      _limit = _DEFAULT_BUFFER_SIZE;
      _bytes = new Uint8List(_limit);
      _buffer = _bytes.buffer;
    } else {
    _initLimits(_buffer.lengthInBytes);
    _bytes = _buffer.asUint8List(_index, _limit);
    }
    _bd = _buffer.asByteData(_index, _limit);
  }

  TypedDataReader.fromList(List list, [this._index, this._limit, this._endian]) {
    _initLimits(list.length);
    _bytes = new Uint8List.fromList(list);
    _buffer = _bytes.buffer;
    _bd = _buffer.asByteData(_index, _limit);
  }

  TypedDataReader.fromUint8List(this._bytes, [this._index = 0, this._limit, this._endian]) {
    _initLimits(_bytes.length);
    _buffer = _bytes.buffer;
    _bd = _buffer.asByteData(index, limit);
  }

  TypedDataReader.fromByteData(this._bd, [this._index, this._limit, this._endian]) {
    _initLimits(_bd.lengthInBytes);
    _buffer = _bd.buffer;
    _bytes = _buffer.asUint8List(_index, _limit);
  }

  // Getters
  int   get index      => _index;
  int   get remaining  => _limit - _index;
  int   get limit      => _limit;
  bool  get isEmpty    => _index >= _limit;
  bool  get isNotEmpty => _index < _limit;

  //***** Debug Help

  Uint8List peak(int offset, int length) => _buffer.asUint8List(offset, length);

  void dbgPrint(offsetInBytes, lengthInBytes) {
    print('current Index= $index');
    int offset = index + offsetInBytes;
    String grp = intToHex(_bd.getUint16(index + 0));
    String elt = intToHex(_bd.getUint16(index + 2));
    String vr  = intToHex(_bd.getUint16(index + 4));
    int slen = _bd.getUint16(index + 6);
    int llen = _bd.getUint32(index + 8);
    Uint8List slist = _bytes.sublist(index + 8, index + 16);
    String sstr = ASCII.decode(slist, allowInvalid: true);
    Uint8List llist = _bytes.sublist(index + 12, index + 20);
    String lstr = ASCII.decode(llist, allowInvalid: true);
    print('Short: $grp $elt $vr $slen');
    print('       $slist');
    print('       $sstr');
    print('Long:  $grp $elt $vr $llen');
    print('       $llist');
    print('       $lstr');
    Uint8List  l8  = _bytes.sublist(offset, offset + lengthInBytes);
    Uint16List l16 = _buffer.asUint16List(offset, lengthInBytes ~/ 2);
   // Uint32List l32 = _buffer.asUint32List(offset, lengthInBytes ~/ 4);
    String     s   = ASCII.decode(l8, allowInvalid: true);

    print('   Uint8  = $l8');
    print('   Uint16 = $l16');
   // print('   Uint32 = $l32');
    print('   String = $s');
  }

  void debug([offsetInBytes = 0, lengthInBytes = 16]) => dbgPrint(offsetInBytes, lengthInBytes);

  //**** Save and Restore Indexes

  SavedIndex saveIndex() => new SavedIndex(_index);
  void       restoreIndex(SavedIndex idx) {
    _index = idx._index;
  }
  //****  Error checking methods  ****

 _initLimits(int length) {
   if (_limit == null) _limit = length;
   if (_index == null) _index = 0;
   if (_endian == null) _endian = Endianness.HOST_ENDIAN;
   if (isEmpty) throw new ArgumentError("(index($_index) >= Limit($_limit");
 }
  /**
   * Check whether moving [_index] by [lengthInBytes], which is either positive or
   * negative, results in a valid [_index].  If so, set [_index] to that value and
   * return it; otherwise, throw [rangeError].
   */
  // TODO should this be private?
  int checkIndex(int lengthInBytes) {
    int pos = _index + lengthInBytes;
    if ((pos < 0) || (pos > _limit)){
      rangeError('Bad index = $pos');
    } else {
      _index = pos;
    }
    return pos;
  }


  /// Checks that [length] is valid positive integer and if so, returns it.
  /// Does not move the [_index]
  int checkLength(int length) {
    if (length < 0) rangeError('length = $length must be positive');
    int pos = _index + length;
    if (pos > _limit) rangeError('length $length is out of range');
    return length;
  }

  /**
   * Returns the [index] that is the local limit for a read of [length].
   */
  int getLimit(int length, {int elementSize: 1}) =>
      _index + checkLength(length * elementSize);

  /// Checks that the [length] is multiple of [sizeInBytes] and if so returns it.
  int checkSize(int lengthInBytes, int sizeInBytes) {
    if ((lengthInBytes % sizeInBytes) != 0)
      parseError('lengthInBytes=$lengthInBytes is not a multiple of $sizeInBytes');
    return checkLength(lengthInBytes) ~/ sizeInBytes;
  }

  //****  Basic reading methods  ****

  /**
   * Moves the [index] forward or backward.
   *
   * Increments [_index] by [n], which may be positive or negative.  An error is
   * thrown if the new [_index] is less than zero (0) or is greater than or equal
   * to [_limit].
   */
  int skip(int n) => checkIndex(n);

  int seek(int n) { _index = n; return n; }

  /// Peek at Uint8 value. Does NOT advance [_index].
  int peekUint8([offset = 0])  => _bd.getUint8(_index + offset);

  /// Peek at Uint16 value. Does NOT advance [_index].
  int peekUint16([offset = 0]) => _bd.getUint16(_index + offset, _endian);

  /// Peek at Uint32 value. Does NOT advance [_index].
  int peekUint32([offset = 0]) => _bd.getUint32(_index + offset, _endian);

  // Basic Signed Integer Readers
  int int8()  { int val = _bd.getInt8(_index);           skip(1); return val; }
  int int16() { int val = _bd.getInt16(_index, _endian); skip(2); return val; }
  int int32() { int val = _bd.getInt32(_index, _endian); skip(4); return val; }
  int int64() { int val = _bd.getInt64(_index, _endian); skip(8); return val; }
  // Basic Unsigned Integer Readers
  int uInt8()  { int val = _bytes[_index]; skip(1);  return val;}
  int readUint16() { int val = _bd.getUint16(_index, _endian); skip(2); return val; }
  int readUint32() { int val = _bd.getUint32(_index, _endian); skip(4); return val; }
  int uInt64() { int val = _bd.getUint64(_index, _endian); skip(8); return val; }
  // Basic Floating Point Readers
  double float32() { double val = _bd.getFloat32(_index, _endian); skip(4); return val; }
  double float64() { double val = _bd.getFloat64(_index, _endian); skip(8); return val; }


// **** Signed Integer List Readers ****

  /// Returns a new [list] of 8-bit _signed_ integers;
  //TODO optimize using slice
  Int8List int8List(int lengthInBytes) {
    int length = checkLength(lengthInBytes);
    Int8List list = new Int8List(length);
    for (var i = 0; i < length; i++) list[i] = int8();
    return list;
  }

  /// Reads 16-bit _signed_ integers from [this] and returns a
  /// new [list] containing them.  The ReadBuffer index is advanced automatically;
  Int16List int16List(int lengthInBytes) {
    int length = checkSize(lengthInBytes, Int16List.BYTES_PER_ELEMENT);
    List list = new Int16List(length);
    for (int i = 0; i < length; i++) list[i] = int16();
    return list;
  }

  /// Reads 32-bit _signed_ integers from [this] and returns a
  /// new [list] containing them.  The ReadBuffer index is advanced automatically;
  Int32List int32List(int lengthInBytes) {
    int length = checkSize(lengthInBytes, Int32List.BYTES_PER_ELEMENT);
    Int32List list = new Int32List(length);
    for (int i = 0; i < length; i++) list[i] = int32();
    return list;
  }

  /// Reads 64-bit _signed_ integers from [this] and returns a
  /// new [list] containing them.  The ReadBuffer index is advanced automatically;
  Int64List readInt64List(int lengthInBytes) {
    int length = checkSize(lengthInBytes, Int64List.BYTES_PER_ELEMENT);
    Int64List list = new Int64List(length);
    for (int i = 0; i < length; i++) list[i] = int64();
    return list;
  }

  // **** Unsigned Integer List Readers ****

  /// Returns a new [list] of 8-bit _unsigned_ integers;
  //TODO optimize? using slice
  Uint8List uInt8List(int lengthInBytes) {
    int nBytes = checkLength(lengthInBytes);
    Uint8List list = new Uint8List(nBytes);
    for (var i = 0; i < nBytes; i++) list[i] = uInt8();
    return list;
  }

  /// Reads 16-bit _unsigned_ integers from [this] and returns a
  /// new [list] containing them.  The ReadBuffer index is advanced automatically;
  Uint16List uInt16List(int lengthInBytes) {
    int length = checkSize(lengthInBytes, Uint16List.BYTES_PER_ELEMENT);
    Uint16List list = new Uint16List(length);
    for (int i = 0; i < length; i++) list[i] = readUint16();
    return list;
  }

  /// Reads 32-bit _unsigned_ integers from [this] and returns a
  /// new [list] containing them.  The ReadBuffer index is advanced automatically;
  Uint32List uInt32List(int lengthInBytes) {
    int length = checkSize(lengthInBytes, Uint32List.BYTES_PER_ELEMENT);
    Uint32List list = new Uint32List(length);
    for (int i = 0; i < length; i++) list[i] = readUint32();
    return list;
  }

  /// Reads 64-bit _unsigned_ integers from [this] and returns a
  /// new [list] containing them.  The ReadBuffer index is advanced automatically;
  Uint64List uInt64List(int lengthInBytes) {
    int length = checkSize(lengthInBytes, Uint64List.BYTES_PER_ELEMENT);
    Uint64List list = new Uint64List(length);
    for (int i = 0; i < length; i++) list[i] = uInt64();
    return list;
  }

  // **** Floating Point List Readers ****
  Float32List float32List(int lengthInBytes) {
    int length = checkSize(lengthInBytes, Float32List.BYTES_PER_ELEMENT);
    Float32List list = new Float32List(length);
    for (int i = 0; i < length; i++) list[i] = float32();
    return list;
  }

  Float64List float64List(int lengthInBytes) {
    int length = checkSize(lengthInBytes, Float64List.BYTES_PER_ELEMENT);
    Float64List list = new Float64List(length);
    for (int i = 0; i < length; i++) list[i] = float64();
    return list;
  }


  /// **** String Readers ****
  //TODO add readUtf8String -> UTF16String

  // Read the next character, but don't advance the index.
  int peekChar() => (!isEmpty) ? uInt8() : emptyBufferError('ReadBuffer[$this] is empty!');

  /// Skips up to [length] spaces in [this].
  int skipSpaces(int length) => skipChars($SPACE, length);

  /// Skips up to [length] characters equal to [char] in [this].
  int skipChars(int char, int length) {
    int limit = getLimit(length);
    int i = 0;
    for ( _index; _index < limit; _index++) {
      if (peekChar() != char) break;
      i++;
    }
    return i;
  }

  /**
    * Read an ASCII [String] with a maximum of [maxLength] characters.  The
    * [delimiter] indicates the end of the string.  The default [delimiter] is
    * [$BACKSLASH].  A [delimiter] of -1 indicates there is no delimiter.
    */
  //TODO should we allow other delimiters or only $BACKSLASH?
  static const NO_STRING_DELIMITER = -1;
  String readString([int maxLength, int delimiter = $BACKSLASH]) {
    print('readString: length=$maxLength');
    int length = (maxLength == null) ? _limit : maxLength;
    return _ascii(length, delimiter);
  }

  // Internal string reader - doesn't check length
  // Read MUST NOT start with [delimiter]
  String _ascii(int maxLength, int delimiter) {
    int start = _index;
    int limit = getLimit(maxLength);
    print('_ascii index=$index, limit=$limit');
    if (delimiter >= 0) {
      for(_index; _index < limit; _index++) {
        int char = _bytes[_index];
        if (char == delimiter) {
          limit = _index;
          _index++;
          break;
        }
      }
    }
    //TODO figure out how to avoid creating this view
    Uint8List bytes = _buffer.asUint8List(start, limit - start);
    return new String.fromCharCodes(bytes);
  }

  /**
    * Reads a [List] of [String]s separated by [delimiter].
    */
  List<String> readStringList(int maxLength,
                              {int maxItemLength: -1,
                               int delimiter: $BACKSLASH}) {
    List<String> list = new List();
    int limit = getLimit(maxLength);
    print('limit=$limit');
    while (_index < limit) {
      maxItemLength = min(limit - _index, maxItemLength);
      String s = _ascii(maxItemLength, delimiter);
      print('in loop: $s');
      list.add(s);
    }
    return list;
  }

  // Read Integers
  //TODO decide if we should use these rather than those below.
  /*
    int readSignedInt(int limit, int min, int max) {
      int sign;
      int char = bytes[index];
      if (char == $PLUS_SIGN) sign = 1;
      else if (char == $MINUS_SIGN) sign = -1;
      if (sign == null) _error("No sign (+/-) found");
      index++;
      int n = _readUint(limit, min, max);
      return sign * n;
    }

    int readInt(int limit, int min, int max) {
      int sign;
      int char = bytes[index];
      if (char == $PLUS_SIGN) sign = 1;
      else if (char == $MINUS_SIGN) sign = -1;
      if (sign != null) index++;
      int n = _readUint(limit, min, max);
      return sign * n;
    }
  */
  // Read a positive integer (radix 10) of exact length nChars.
  int readPositiveInteger(int nChars, int min, int max) {
    int limit = getLimit(nChars);
    //TODO finish
    return _readPositiveInteger(_index, limit, min, max);
  }

  // Read a positive integer (radix 10) of exact length nChars.
  int _readPositiveInteger(int start, int end, int min, int max) {
    int n = 0;
    for(int i = start; i < end; i++) {
      var char = peekChar();
      if (isDigit(char)) {
        n = (n * 10) + digitValue(char);
        _index++;
      } else if (char == $SPACE) {
        for(i+1; i < end; i++) {
          if (char != $SPACE) parseError('Invalid Char=$char after trailing SPACE');
          if (n == 0) return null;
        }
      } else parseError('invalid CodeUnit= $char');
    }
    if ((n < min) || (n > max)) parseError('value out of range');
    return n;
  }

  /**
    * Reads a [List] of [int]s from [this]
    */
  List<int> readIntegerList(int maxLength,
                            {int maxItemLength: 12,
                             int delimiter: $BACKSLASH}) {
    List list = new List();
    int limit = getLimit(maxLength);
    while (_index < limit) {
      list.add(readInteger(min(limit, maxItemLength)));
      skipChars(delimiter, 1);
    }
    return list;
  }

  /**
    * Read an integer from [this].
    *
    * Note: The integer might have leading or training spaces, which are ignored.
    *       When writing the numbers as code units no leading or trailing
    *       whitespace (SPACE only) should be written.
    */
  //TODO This may not skip enough leading or trailing spaces.
  int readInteger(int maxLength) {
    int limit = getLimit(maxLength);
    int n = skipSpaces(remaining);
    n = _readIntegerPart(limit);
    if (_index != limit) skipSpaces(remaining);
    // we shouldn't have to skip nulls
    return n;
  }

  /**
    *  Check for leading '+' or '-' and return +1 or -1.
    */
  int _readSign() {
    int char = peekChar();
    if (char == $MINUS_SIGN) {
      _index++;
      return -1;
    } else if (char == $PLUS_SIGN) {
      _index++;
      return 1;
    } else return 1;
  }

  /**
    * Read the integer part of a number (integer or  decimal) number from a Uint8List.
    *
    * Note: The number (integer or decimal) might have leading or training
    *       spaces.  When writing the numbers as code units no leading or
    *       trailing whitespace should be written.
    */
  int _readIntegerPart(int limit) {
    int value = 0;
    int sign = _readSign();
    while (_index < limit) {
      var char = peekChar();
      if (isDigit(char)) {
        value = (value * 10) + digitValue(char);
        _index++;
      } else break;
    }
    return sign * value;
  }


  /**
    * Read the fractional part of a decimal number
    *
    * The fractional part MUST start with a _PERIOD (decimal point).
    */
  num readFractionalPart(int limit) {
    num base = 1;
    num value = 0.0;
    if (peekChar() != $PERIOD) return 0;
    _index++;
    while (_index < limit) {
      int char = peekChar();
      if (isDigit(char)) {
        base *= 10;
        value = (value + (digitValue(char) / base));
        _index++;
      } else {
        //TODO do we need to check for an error here?
        break;
      }
    }
    return value;
  }

  int _readExponentPart(int limit) {
    int value = 1;
    int char = peekChar();
    if ((char == $E) || (char == $e)) {
      _index++;
      value = _readIntegerPart(limit);
    } else {
      //There is no exponent
      return 1;
    }
    return pow(10, value);
  }

  /**
    * Reads the bytes as characters and reads a decimal number starting at
    * _idx.  The number has the format [sign]int[.int][Eint].  [maxLength]
    * specifies the maximum length of the decimal string. This method should not
    * read more than [maxLength] characters.
    */
  //TODO make sure that length is not exceeded
  num readDecimal([int maxLength = 16]) {
    int limit = getLimit(maxLength);
    skipSpaces(remaining);
    int i = _readIntegerPart(limit);
    num f = 0;
    num e = 1;
    if (_index < limit) f = readFractionalPart(limit);
    if (_index < limit) e = _readExponentPart(limit);
    skipSpaces(remaining);
    //TODO Should not need to skip null
    if (i < 0) {
      f = (i - f);
    } else {
      f = (i + f);
    }
    return f * e;
  }

  List<num> readDecimalList(int length,
                            {int delimiter: $BACKSLASH,
                              //TODO what should the default be on the next line
                             int maxItemLength: 16}) {
    List<num> list = new List();
    int limit = getLimit(length);
    while (_index < limit) {
      //Note: the maxLength of VR.DS (decimal string) is 16
      list.add(readDecimal(min(limit, 16)));
      //TODO verify that this generates an error if no backslash
      skipChars($BACKSLASH, 1);
    }
    return list;
  }

  /**
    * Read a Universally Unique Identifier (UUID)
    * See http://docs.oracle.com/javase/7/docs/api/java/util/UUID.html
    */
  //TODO this should check for backslash or other delimiter
  List<Uuid> readUuidList({maxLength: 16, int delimiter: $BACKSLASH}) {
    List list = new List();
    for (var i = 0; i < maxLength; i++) {
      list.add(readUuid());
    }
    return list;
  }

  Uuid readUuid() => new Uuid.fromList(uInt8List(16));

  /**
    * Read a OSI Object Identification UID
    * Based on the OSI Object Identification (numeric form) as defined by
    * the ISO 8824 standard.  See DICOM PS3.5 Section 9 (page 61 in .pdf)
    */
  //TODO should this be returning a UID object rather than a string?
  UID readUid({maxLength: 64}) {
    checkLength(maxLength);
    var s = readString(maxLength);
    // Uids can be padded with 0 to make the string end on a 2 byte boundary.
    int len = s.length;
    if (len.isEven) {
      int idx = len - 1;
      int code = s.codeUnitAt(idx);
      if (code == 0) s = s.substring(0, idx);
    }
    return new UID.fromString(s);
  }

  //TODO finish and test
  List<String> readUidList(length, {int maxItemLength: 64}) {
    int limit = getLimit(length);
    List list = new List();
    for (var i = _index; i < limit; i++) {
      list.add(readUid(maxLength: min(length, maxItemLength)));
      i = _index;
    }
    return list;
  }

  //TODO this should be checking the length
  Uri readUri(int maxLength) => Uri.parse(readString(maxLength));

}
