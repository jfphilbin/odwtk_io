// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library read_buffer_simple;

/*
 * A simple reader for TypedData.
 *
 * This reader uses the ByteBuffer.as... to ensure safety.  It may not be the
 * fastest reader!
 */
import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:io/io_error.dart';
import 'package:utilities/ascii.dart';
import 'package:utilities/uuid.dart';

typedef dynamic Slicer(int length);

/**
 * Read [bytes] from a [ByteData] buffer.
 */
class ReadBuffer {
  static const _DEFAULT_BUFFER_SIZE = 4096;
  ByteBuffer _buffer;
  int        _index;
  int        _limit;    // When _index == _limit the buffer is empty.
  Uint8List  _uint8;
  ByteData   _bd;

  Endianness _endian = Endianness.HOST_ENDIAN;

  // Getters
  int  get index      => _index;
  int  get remaining  => _limit - _index;
  int  get limit      => _limit;
  bool get isEmpty    => _index >= _limit;
  bool get isNotEmpty => _index < _limit;

  //TODO useful? bool isNull(Object value) => value == null;

  /// Constructors
  ReadBuffer([this._buffer, this._index = 0, this._limit, this._endian]) {
    _initBuffer(_buffer, _index, _limit, _uint8, _bd, _endian);
  }

  ReadBuffer.fromUint8List(this._uint8, [this._index = 0, this._limit, this._endian]) {
    _initBuffer(_uint8.buffer, _index, _limit, _uint8, null, _endian);
  }

  ReadBuffer.fromByteData(this._bd, [this._index = 0, this._limit, this._endian]) {
    _initBuffer(_bd.buffer, _index, _limit, null, _bd, _endian);
  }

  /// ReadBuffer Initialization
  void _initBuffer(ByteBuffer bb, int index, int limit, Uint8List uint8, ByteData bd,
                   Endianness endian ) {
    if (bb == null) {
      _limit = _DEFAULT_BUFFER_SIZE;
      _uint8 = new Uint8List(_limit);
      _buffer = _uint8.buffer;
    } else {
      _limit = (limit == null) ? _buffer.lengthInBytes : limit;
    }
    if (isEmpty) throw new ArgumentError("(index($_index) >= Limit($_limit");
    if (uint8 == null) _uint8 = _buffer.asUint8List(index, limit);
    if (bd == null)    _bd    = _buffer.asByteData(index, limit);
  }

  // Increment _index by count.  An error is thrown if count is not a positive integer
  // or if the new _index is greater than or equal to _limit.
  int _inc(int count) {
    if (count < 1) _rangeError('count=$count must be a positive integer');
    _index = _index + count;
    if (_index > _limit) _rangeError('index=$_index out of range');
    return _index;
  }

  // Errors
  void _rangeError(String msg) => throw new RangeError(msg);

  void _emptyBufferError(String msg) => throw new ArgumentError(msg);

  // Basic Readers
  int int8()  { int val = _bd.getInt8(_index);           _inc(1); return val; }
  int int16() { int val = _bd.getInt16(_index, _endian); _inc(2); return val; }
  int int32() { int val = _bd.getInt32(_index, _endian); _inc(4); return val; }
  int int64() { int val = _bd.getInt64(_index, _endian); _inc(8); return val; }

  double float32() { double val = _bd.getFloat32(_index, _endian); _inc(4); return val; }
  double float64() { double val = _bd.getFloat64(_index, _endian); _inc(8); return val; }

  int uInt8()  { int val = _uint8[_index]; _inc(1);  return val;}
  int uInt16() { int val = _bd.getUint16(_index, _endian); _inc(2); return val; }
  int uInt32() { int val = _bd.getUint32(_index, _endian); _inc(4); return val; }
  int uInt64() { int val = _bd.getUint64(_index, _endian); _inc(8); return val; }

  // Peek at Uint8 or Uint16 values, but does not advance [_index].
  int peek8()  => _bd.getUint8(_index);
  int peek16() => _bd.getUint16(_index, _endian);


// **** Internal Helper methods

  // Checks that the [_buffer] has at least [length] * [sizeInBytes] bytes remaining.
  // Returns the new [_buffer] [_limit]
  int _chkLength(int length, [ int sizeInBytes = 1]) {
    int limit = _index + (length * sizeInBytes);
    if (limit > _limit) _rangeError('length $length is out of range');
    return limit;
  }

  /**
   * Returns the [index] that is the local limit for a read of [length].
   */
  int localLimit(int length, {int elementSize: 1}) => _checkLimit(length * elementSize);

  /// Checks that [lengthInBytes] is valid and if so, returns local limit.
  int _checkLimit(int lengthInBytes) {
    int limit = _index + lengthInBytes;
    if (limit > _limit) _rangeError('length $lengthInBytes exceeds buffer size');
    return limit;
  }

  //TODO needed?
  int checkSize(int length, int sizeInBytes) => _chkSize(length, sizeInBytes);

  /// Checks that the [length] is multiple of [sizeInBytes] and if so returns it.
  int _chkSize(int lengthInBytes, int sizeInBytes) {
    if ((lengthInBytes % sizeInBytes) != 0)
      parseError('lengthInBytes=$lengthInBytes is not a multiple of $sizeInBytes');
    return _chkLength(lengthInBytes) ~/ sizeInBytes;
  }

// **** Binary List Readers ****

  dynamic _getSlice(Slicer slicer, int length, int sizeInBytes) {
    int limit = _index + (length * sizeInBytes);
    if (limit > _limit) _rangeError('length $length is out of range');
    _index = limit;
    return slicer(length);
  }

  // Signed Integer Lists
  Int16List int8List(int  length) => _getSlice(_buffer.asInt8List,  length, 1);
  Int16List int16List(int length) => _getSlice(_buffer.asInt16List, length, 2);
  Int16List int32List(int length) => _getSlice(_buffer.asInt32List, length, 4);
  Int16List int64List(int length) => _getSlice(_buffer.asInt64List, length, 8);

  // Unsigned Integer Lists
  Int16List uint8List(int  length) => _getSlice(_buffer.asUint8List,  length, 1);
  Int16List uint16List(int length) => _getSlice(_buffer.asUint16List, length, 2);
  Int16List uint32List(int length) => _getSlice(_buffer.asUint32List, length, 4);
  Int16List uint64List(int length) => _getSlice(_buffer.asUint64List, length, 8);

  // Float Lists
  Int16List float32List(int length) => _getSlice(_buffer.asFloat32List, length, 4);
  Int16List float64List(int length) => _getSlice(_buffer.asFloat64List, length, 8);

  /// **** String Readers ****
  //TODO add readUtf8String -> UTF16String

  // Read the next character, but don't advance the index.
  int _peek() => _bd.getUint8(_index);

  int peek() => (!isEmpty) ? _peek() : _emptyBufferError('ReadBuffer[$this] is empty!');

  // Skip over [n] bytes
  int skip(int n) => _index += _chkLength(n);

  /// Skips up to [length] spaces in [this].
  int skipSpaces(int length) => skipChars($SPACE, length);

  /// Skips up to [length] characters equal to [char] in [this].
  int skipChars(int char, int length) {
    int limit = _checkLimit(length);
    int i = 0;
    for ( _index; _index < limit; _index++) {
      if (_peek() != char) break;
      i++;
    }
    return i;
  }
  /**
    * Read an ASCII [String] with a maximum of [maxLength] characters.  The
    * [delimiter] indicates the end of the string.  The default [delimiter] is
    * [$BACKSLASH].  A [delimiter] of -1 indicates there is not delimiter.
    */
  String readString([int maxLength, int delimiter = $BACKSLASH]) {
    int length = (maxLength == null) ? _limit : maxLength;
    return _ascii(length, delimiter);
  }

  int nextDelimiter(int delimiter, int limit) {
    for(int i = _index; i < limit; i++) {
      if (_uint8[i] == delimiter) return i;
    }
    return limit;
  }

  // Internal string reader - doesn't check length
  // Read MUST NOT start with [delimiter]
  String _ascii(int maxLength, int delimiter) {
    int start = _index;
    int limit = _chkLength(maxLength);
    int end = nextDelimiter(delimiter, limit);
    return ASCII.decode(_buffer.asUint8ClampedList(start, end));
  }

  /**
    * Reads a [List] of [String]s separated by [delimiter].
    */
  List<String> readStringList(int maxLength,
                              {int maxItemLength: -1,
                               int delimiter: $BACKSLASH}) {
    List<String> list = new List();
    int limit = _checkLimit(maxLength);
    while (_index < limit) {
      maxItemLength = min(limit - _index, maxItemLength);
      String s = _ascii(maxItemLength, delimiter);
      list.add(s);
    }
    return list;
  }

  /**
    * Reads a [List] of [int]s from [this]
    */
  List<int> readIntegerList(int maxLength,
                            {int maxItemLength: 12,
                             int delimiter: $BACKSLASH}) {
    List list = new List();
    int limit = _checkLimit(maxLength);
    while (_index < limit) {
      list.add(readInteger(min(limit, maxItemLength)));
      skipChars(delimiter, 1);
    }
    return list;
  }
  //TODO decide if we should use these rather than those below.
  /*
    int readSignedInt(int limit, int min, int max) {
      int sign;
      int char = bytes[index];
      if (char == $PLUS_SIGN) sign = 1;
      else if (char == $MINUS_SIGN) sign = -1;
      if (sign == null) _error("No sign (+/-) found");
      index++;
      int n = _readUint(limit, min, max);
      return sign * n;
    }

    int readInt(int limit, int min, int max) {
      int sign;
      int char = bytes[index];
      if (char == $PLUS_SIGN) sign = 1;
      else if (char == $MINUS_SIGN) sign = -1;
      if (sign != null) index++;
      int n = _readUint(limit, min, max);
      return sign * n;
    }
  */
  // Read a positive integer (radix 10) of exact length nChars.
  int readPositiveInteger(int nChars, int min, int max) {
    int limit = _checkLimit(nChars);
    //TODO finish
    return _readPositiveInteger(_index, limit, min, max);
  }

  // Read a positive integer (radix 10) of exact length nChars.
  int _readPositiveInteger(int start, int end, int min, int max) {
    int n = 0;
    for(int i = start; i < end; i++) {
      var char = _peek();
      if (isDigit(char)) {
        n = (n * 10) + digitValue(char);
        _index++;
      } else if (char == $SPACE) {
        for(i+1; i < end; i++) {
          if (char != $SPACE) parseError('Invalid Char=$char after trailing SPACE');
          if (n == 0) return null;
        }
      } else parseError('invalid CodeUnit= $char');
    }
    if ((n < min) || (n > max)) parseError('value out of range');
    return n;
  }

  /**
    * Read an integer from [this].
    *
    * Note: The integer might have leading or training spaces, which are ignored.
    *       When writing the numbers as code units no leading or trailing
    *       whitespace (SPACE only) should be written.
    */
  //TODO This may not skip enough leading or trailing spaces.
  int readInteger(int maxLength) {
    int limit = _checkLimit(maxLength);
    int n = skipSpaces(remaining);
    n = _readIntegerPart(limit);
    if (_index != limit) skipSpaces(remaining);
    // we shouldn't have to skip nulls
    return n;
  }

  /**
    *  Check for leading '+' or '-' and return +1 or -1.
    */
  int _readSign() {
    int char = _peek();
    if (char == $MINUS_SIGN) {
      _index++;
      return -1;
    } else if (char == $PLUS_SIGN) {
      _index++;
      return 1;
    } else return 1;
  }

  /**
    * Read the integer part of a number (integer or  decimal) number from a Uint8List.
    *
    * Note: The number (integer or decimal) might have leading or training
    *       spaces.  When writing the numbers as code units no leading or
    *       trailing whitespace should be written.
    */
  int _readIntegerPart(int limit) {
    int value = 0;
    int sign = _readSign();
    while (_index < limit) {
      var char = _peek();
      if (isDigit(char)) {
        value = (value * 10) + digitValue(char);
        _index++;
      } else break;
    }
    return sign * value;
  }

  /**
    * Read the fractional part of a decimal number
    *
    * The fractional part MUST start with a _PERIOD (decimal point).
    */
  num readFractionalPart(int limit) {
    num base = 1;
    num value = 0.0;
    if (_peek() != $PERIOD) return 0;
    _index++;
    while (_index < limit) {
      int char = _peek();
      if (isDigit(char)) {
        base *= 10;
        value = (value + (digitValue(char) / base));
        _index++;
      } else {
        //TODO do we need to check for an error here?
        break;
      }
    }
    return value;
  }

  int _readExponentPart(int limit) {
    int value = 1;
    int char = _peek();
    if ((char == $E) || (char == $e)) {
      _index++;
      value = _readIntegerPart(limit);
    } else {
      //There is no exponent
      return 1;
    }
    return pow(10, value);
  }

  /**
    * Reads the bytes as characters and reads a decimal number starting at
    * _idx.  The number has the format [sign]int[.int][Eint].  [maxLength]
    * specifies the maximum length of the decimal string. This method should not
    * read more than [maxLength] characters.
    */
  //TODO make sure that length is not exceeded
  num readDecimal([int maxLength = 16]) {
    int limit = _checkLimit(maxLength);
    skipSpaces(remaining);
    int i = _readIntegerPart(limit);
    num f = 0;
    num e = 1;
    if (_index < limit) f = readFractionalPart(limit);
    if (_index < limit) e = _readExponentPart(limit);
    skipSpaces(remaining);
    //TODO Should not need to skip null
    if (i < 0) {
      f = (i - f);
    } else {
      f = (i + f);
    }
    return f * e;
  }

  List<num> readDecimalList(int length,
                            {int delimiter: $BACKSLASH,
                              //TODO what should the default be on the next line
                             int maxItemLength: 16}) {
    List<num> list = new List();
    int limit = _checkLimit(length);
    while (_index < limit) {
      //Note: the maxLength of VR.DS (decimal string) is 16
      list.add(readDecimal(min(limit, 16)));
      //TODO verify that this generates an error if no backslash
      skipChars($BACKSLASH, 1);
    }
    return list;
  }

  /**
    * Read a Universally Unique Identifier (UUID)
    * See http://docs.oracle.com/javase/7/docs/api/java/util/UUID.html
    */
  //TODO this should check for backslash or other delimiter
  List<Uuid> readUuidList({maxLength: 16, int delimiter: $BACKSLASH}) {
    List list = new List();
    for (var i = 0; i < maxLength; i++) {
      list.add(readUuid());
    }
    return list;
  }

  Uuid readUuid() => new Uuid.fromList(new Uint8List(16));

  /**
    * Read a OSI Object Identification UID
    * Based on the OSI Object Identification (numeric form) as defined by
    * the ISO 8824 standard.  See DICOM PS3.5 Section 9 (page 61 in .pdf)
    */
  //TODO should this be returning a UID object rather than a string?
  String readUid({maxLength: 64}) {
    _checkLimit(maxLength);
    String uidStr = readString(maxLength);
    //TODO make this next line work
    //UidUtils.isValid(uidStr);
    return uidStr;
  }

  //TODO finish and test
  List<String> readUidList(length, {int maxItemLength: 64}) {
    int limit = _checkLimit(length);
    List list = new List();
    for (var i = _index; i < limit; i++) {
      list.add(readUid(maxLength: min(limit, maxItemLength)));
    }
    return list;
  }

  //TODO this should be checking the length
  Uri readUri(int maxLength) => Uri.parse(readString(maxLength));


}