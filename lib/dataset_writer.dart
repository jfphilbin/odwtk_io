// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library DatasetWriter;

//import 'dart:convert' as Convert;
//import 'dart:math';
import 'dart:typed_data';

import 'package:io/io_error.dart';
//import 'package:utilities/ascii.dart';
import 'package:utilities/uuid.dart';