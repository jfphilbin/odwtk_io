// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Author: James F Philbin <james.philbin@jhmi.edu>
library typed_data_writer;

import 'dart:typed_data';

import 'package:io/io.dart';

/// Typedefs used to create a closure used to write a length into a buffer after
/// subsequent data has been written
typedef void WriteLength(int length);
typedef WriteLength LengthWriter(int index);

//TODO document
//TODO should this be private?
//TODO where is ConstructedConstant defined.
//ConstructedConstant(Endianness("_littleEndian"=Instance of 'FalseConstant'));

/**
 *
 */
class TypedDataWriter {
  static const _DEFAULT_BUFFER_SIZE = 8192;
  ByteBuffer _buffer;
  int _index = 0;
  int _limit; // When _index == _limit the buffer is empty.
  Uint8List _bytes;
  ByteData _bd;
  Endianness _endian = Endianness.HOST_ENDIAN; // The default endianness

  // Constructors
  TypedDataWriter([ByteBuffer this._buffer, this._index = 0, this._limit, this._endian]) {
    if (_buffer == null) {
      // No need to checkLimit here.
      _limit = _DEFAULT_BUFFER_SIZE;
      _bytes = new Uint8List(_limit);
      _buffer = _bytes.buffer;
    } else {
      _initLimits(_buffer.lengthInBytes);
      _bytes = _buffer.asUint8List(_index, _limit);
    }
    _bd = _buffer.asByteData(_index, _limit);
  }

  TypedDataWriter.fromList(List bytes, [this._index = 0, this._limit, this._endian]) {
    _bytes = new Uint8List.fromList(bytes);
    _initLimits(_bytes.lengthInBytes);
    _buffer = _bytes.buffer;
    _bd = _buffer.asByteData(_index, _limit);
  }

  TypedDataWriter.fromUint8List(Uint8List this._bytes, [this._index = 0, this._limit, this._endian])
      {
    _initLimits(_bytes.lengthInBytes);
    _buffer = _bytes.buffer;
    _bd = _buffer.asByteData(_index, _limit);
  }

  TypedDataWriter.fromByteData(ByteData this._bd, [this._index = 0, this._limit, this._endian]) {
    _initLimits(_bd.lengthInBytes);
    _buffer = _bd.buffer;
    _bytes = _buffer.asUint8List(_index, _limit);
  }

  // Getters
  int get index => _index;
  int get remaining => _limit - _index;
  int get limit => _limit;
  bool get isEmpty => _index >= _limit;
  bool get isNotEmpty => _index < _limit;

  // Errors and Error checking
  _initLimits(int length) {
    if (_limit == null) _limit = length;
    if (isEmpty) throw new ArgumentError("(index($_index) >= Limit($_limit");
  }

  /**
   * Check whether moving [_index] by [lengthInBytes], which is either positive or
   * negative, results in a valid [_index].  If so, set [_index] to that value and
   * return it; otherwise, throw [rangeError].
   */
  int checkIndex(int lengthInBytes) {
    int pos = _index + lengthInBytes;
    if ((pos < 0) || (pos > _limit)) {
      rangeError('Bad index = $pos');
    } else {
      _index = pos;
    }
    return pos;
  }

  /**
   * Checks that [length] is valid positive integer and if so, returns it.
   * Does not move the [_index].
   *
   */
  int checkLength(int length) {
    if (length < 0) rangeError('length = $length must be positive');
    int pos = _index + length;
    if (pos > _limit) rangeError('length $length is out of range');
    return length;
  }

  /**
   * Returns the [index] that is the local limit for a read of [length].
   */
  int getLimit(int length, {int elementSize: 1}) => _index + checkLength(length * elementSize);

  /// Checks that the [length] is multiple of [sizeInBytes] and if so returns it.
  int checkSize(int lengthInBytes, int sizeInBytes) {
    if ((lengthInBytes % sizeInBytes) !=
        0) parseError('lengthInBytes=$lengthInBytes is not a multiple of $sizeInBytes');
    return checkLength(lengthInBytes) ~/ sizeInBytes;
  }

  /**
   * Create a closure over a location in the buffer.  When call with a length it
   * writes the length into the saved location.
   */
  LengthWriter getLengthWriter(int index) => ((int length) {
    _bd.setUint32(index, length, _endian);
  });

  /**
   * Moves the [index] forward or backward.
   *
   * Increments [_index] by [n], which may be positive or negative.  An error is
   * thrown if the new [_index] is less than zero (0) or is greater than or equal
   * to [_limit].
   */
  int skip(int n) => checkIndex(n);

  // Signed Integer methods
  void writeInt8(int val) {
    _bd.setInt8(_index, val);
    skip(1);
  }

  void writeInt16(int val) {
    _bd.setInt16(_index, val, _endian);
    skip(2);
  }
  void writeInt32(int val) {
    _bd.setInt32(_index, val, _endian);
    skip(4);
  }
  void writeInt64(int val) {
    _bd.setInt64(_index, val, _endian);
    skip(8);
  }

  // Unsigned Interger methods
  void writeUint8(int val) {
    _bd.setUint8(_index, val);
    skip(1);
  }
  void writeUint16(int val) {
    _bd.setUint16(_index, val, _endian);
    skip(2);
  }
  void writeUint32(int val) {
    _bd.setUint32(_index, val, _endian);
    skip(4);
  }
  void writeUint64(int val) {
    _bd.setUint64(_index, val, _endian);
    skip(8);
  }

  // Floating Point methods
  void writeFloat32(double val) {
    _bd.setFloat32(_index, val, _endian);
    skip(4);
  }
  void writeFloat64(double val) {
    _bd.setFloat64(_index, val, _endian);
    skip(8);
  }

  // **** Signed Integer List Readers ****

  // Write [Int8List] to buffer.
  void writeInt8List(Int8List list) {
    int length = checkLength(list.lengthInBytes);
    for (var i = 0; i < length; i++) writeInt8(list[i]);
  }

  /// Reads 16-bit _signed_ integers from [this] and returns a
  /// new [list] containing them.  The ReadBuffer index is advanced automatically;
  void writeInt16List(Int16List list) {
    checkLength(list.lengthInBytes);
    for (int i = 0; i < list.length; i++) writeInt16(list[i]);

  }

  /// Reads 32-bit _signed_ integers from [this] and returns a
  /// new [list] containing them.  The ReadBuffer index is advanced automatically;
  void writeInt32List(Int32List list) {
    checkLength(list.lengthInBytes);
    for (int i = 0; i < list.length; i++) writeInt32(list[i]);;
  }

  /// Reads 64-bit _signed_ integers from [this] and returns a
  /// new [list] containing them.  The ReadBuffer index is advanced automatically;
  void writeInt64List(Int64List list) {
    checkLength(list.lengthInBytes);
    for (int i = 0; i < list.length; i++) writeInt64(list[i]);
  }

  // **** Unsigned Integer List Readers ****

  /// Returns a new [list] of 8-bit _unsigned_ integers;
  void writeUint8List(Uint8List list) {
    int length = checkLength(list.lengthInBytes);
    for (var i = 0; i < length; i++) writeUint8(list[i]);
  }

  /// Reads 16-bit _unsigned_ integers from [this] and returns a
  /// new [list] containing them.  The ReadBuffer index is advanced automatically;
  void writeUint16List(Uint16List list) {
    checkLength(list.lengthInBytes);
    for (int i = 0; i < list.length; i++) writeUint16(list[i]);
  }

/// Reads 32-bit _unsigned_ integers from [this] and returns a
/// new [list] containing them.  The ReadBuffer index is advanced automatically;
  void writeUint32List(Uint32List list) {
    checkLength(list.lengthInBytes);
    for (int i = 0; i < list.length; i++) writeUint32(list[i]);
  }

/// Reads 64-bit _unsigned_ integers from [this] and returns a
/// new [list] containing them.  The ReadBuffer index is advanced automatically;
  void writeUint64List(Uint64List list) {
    checkLength(list.lengthInBytes);
    for (int i = 0; i < list.length; i++) writeUint64(list[i]);
  }

// **** Floating Point List Readers ****
   void writeFloat32List(Float32List list) {
    checkLength(list.lengthInBytes);
    for (int i = 0; i < list.length; i++) writeFloat32(list[i]);
  }

  void writeFloat64List(Float64List list) {
    checkLength(list.lengthInBytes);
    for (int i = 0; i < list.length; i++) writeFloat64(list[i]);
  }


}
