// DCMiD Project
// Copyright 2014 Johns Hopkins University
// Authors: James F Philbin <james.philbin@jhmi.edu>
//          Damish Shah     <dshah15@jhu.edu>
//          Danielle Tinio  <dtinio1@jhu.edu>

library bytebuf;

import 'dart:convert';
import 'dart:math';
import 'dart:typed_data';

import 'package:utilities/utilities.dart';
//import 'package:core/uid_util.dart';
//import 'package:uuid/uuid_server.dart';

//TODO All public methods should check their args for end of buffer.

class ByteBuf {
  ByteBuffer _buffer;
  ByteData _bd;
  Uint8List _uint8List;
  int _base;
  int _capacity;
  int _rdIdx;
  int _wrIdx;
  Endianness _endianness = Endianness.LITTLE_ENDIAN;
  //final Uuid uuid = new Uuid();

  //TODO After performance analysis
  //    1. Determine if we should preallocate the list size after performance analysis
  //    2. Create a cache of ByteBufs used by the factories if needed

  /// Constructors

  ByteBuf(this._bd, this._rdIdx, this._wrIdx,
          [this._endianness = Endianness.LITTLE_ENDIAN]) {
    _base = _bd.offsetInBytes;
    _capacity = _bd.lengthInBytes - _bd.offsetInBytes;
    _uint8List = new Uint8List.view(_bd.buffer, _base, _capacity);
  }

  factory ByteBuf.from(Uint8List bytes) {
    //Not sure how to read something as a ByteData without using a future.
    ByteData bd = new ByteData.view(bytes.buffer, 0, bytes.lengthInBytes);
    return new ByteBuf(bd, 0, bd.lengthInBytes);
  }

  factory ByteBuf.empty(int length) {
    return new ByteBuf(new ByteData(length), 0, 0);
  }

  /**
   * Returns a new [ByteBuf] that is a slice of [this]
   */
  ByteBuf slice(int offset, int length) =>
      new ByteBuf(_bd, _chkRdIdx(offset), _chkRdIdx(length));

  ByteData get byteData => _bd;
  Uint8List get uint8list => _uint8List;
  int get base => _bd.offsetInBytes;
  int get capacity => _bd.lengthInBytes;
  int get rdRemaining => (_wrIdx - _rdIdx);
  bool get isEmpty => (_rdIdx >= _wrIdx);
  bool get isNotEmpty => (_rdIdx < _wrIdx);

  bool isReadable(int length) => rdRemaining >= length;

  void set byteData(ByteData bd) {
    _bd = bd;
    _base = bd.offsetInBytes;
    _capacity = bd.lengthInBytes;
    _uint8List = new Uint8List.view(_bd.buffer, _base, _capacity);
  }

  int get rdIndex => _rdIdx;
  void set rdIndex(int i) { _rdIdx = _chkRdIdx(i); }

  /**
   * Sets the readIdx and writeIdx = 0.
   */
  void clear() {
    _rdIdx = 0;
    _wrIdx = 0;
  }

  /**
   * Sets the [readIdx] and [writeIdx] at the same time.
   */
  void setIndex(int readIdx, int writeIdx) {
    _rdIdx = _chkRdIdx(readIdx);
    _wrIdx = _chkWrIdx(readIdx);
  }

  /**
   * Validators for [index] and [length] arguments.
   *
   * If the [index] or [length] is valid, returns that value; otherwise,
   * throws the appropriate [RangeError].
   */
  int _chkRdIdx(int index) {
    return ((index < 0) || (index > _wrIdx)) ? _rangeError(
        'Index:$index out of range $_rdIdx..$_wrIdx.') : index;
  }

  int _checkRdLen(int byteLength, [int elementSize = 1]) {
    if (byteLength % elementSize != 0) parseError("Length must be a multiple of elementSize");
    int length = byteLength ~/ elementSize;
    return (byteLength > rdRemaining) ? _rangeError(
        'Length=$byteLength exceeds remaining=$rdRemaining.') : length;
  }

  /// Peek or Read a Byte
  //TODO Are these necessary?
  // I don't this this next statement is true once we overload the [] operator.
  // These two are fast because they do no error checking.
  int _peek() => _uint8List[_rdIdx];
  int _read() {
    int char = _uint8List[_rdIdx];
    _rdIdx++;
    return char;
  }

  // These check that the read index is in range.
  int peek() {
    _chkRdIdx(_rdIdx);
    return _peek();
  }
  int read() {
    _chkRdIdx(_rdIdx);
    return _read();
  }
  int operator [](int _rdIdx) => _uint8List[_chkRdIdx(_rdIdx)];

  // Set and Check the Read Limit for the method that calls it.
  // All public methods should use these methods
  /**
   * Sets the limit for a read to:
   *  1) if length == -1 then sets to the size of rdRemaining
   *  2) if length > rdRemaining then set it to rdRemaining
   *  3) else sets it to length.
   */
  int readLimit(int length) => _rdLimit(length);

  int _rdLimit(int length) {
    if ((length > rdRemaining) || (length == -1) || (length == null)) {
      return _wrIdx;
    } else {
      return _rdIdx + length;
    }
  }

  Uint8List readView(int length) {
    int size = _checkRdLen(length, _int8Size);
    Uint8List val = new Uint8List.view(_bd.buffer, _rdIdx, length);
    _rdIdx += length;
    return val;
  }

  /**
   *  Skip over [n] bytes
   */
  int skip(int n) => _chkRdIdx(this._rdIdx += n);

  /**
   * Skips up to [n] chars.
   */
  int skipChars(int char, int n) {
    int count = _chkRdIdx(_rdLimit(n)-_rdIdx);
    int i;
    for (i = 0; i < count; i++) {
      if (_peek() == char) {
        //print("Skipped Char [$char] at: $_rdIdx");
        _rdIdx++;
      } else break;
    }
    return i;
  }

  //DELETE THIS PUBLIC METHOD
  String uint8ToString(int offset, int length) {
    Uint8List bytes = new Uint8List.view(_uint8List.buffer, offset, length);
    return ASCII.decode(bytes);
  }
  //

  /**
   * Read a [String] of [Length].
   */
  //Need to figure out if we should use optional named parameters here.
  //String readString({int maxLength: -1, int delimiter: $BACKSLASH}) =>
  String readString([int maxLength = -1, int delimiter = $BACKSLASH]) =>
      _readString(maxLength, delimiter);

  String _readString(int maxLength, int delimiter) {
    int start = _rdIdx;
    int limit = _rdLimit(maxLength);

    if (delimiter >= 0)
      while (_rdIdx < limit) {
        int char = _uint8List[_rdIdx];
        if (char != delimiter && char != null) _rdIdx++;
        else {
          limit = _rdIdx;
          _rdIdx++;
          break;
        }
      }

    //TODO Figure out how to avoid creating this view
    Uint8List bytes = new Uint8List.view(_uint8List.buffer, start, limit - start);
    return ASCII.decode(bytes);
  }

  /**
   * Read a [List] of [String]s seperated by [delimiter].
   */
  List<String> readStringList(int length, int maxItemLength, [int delimiter = $BACKSLASH]) {
    List<String> list = new List();
    int start = _rdIdx;
    int limit = _rdLimit(length);

    for(_rdIdx; _rdIdx < limit; _rdIdx++) {
      if (_uint8List[_rdIdx] == $BACKSLASH) {
        list.add(uint8ToString(start, _rdIdx-start));
        start = _rdIdx + 1;
      } else if (_rdIdx == limit - 1) {
        if (_uint8List[_rdIdx] == 0 || _uint8List[_rdIdx] == $SPACE) limit = limit - 1;
        list.add(uint8ToString(start, limit-start));
      }
    }

    return list;

    //TODO Get this working.
    /*
    List<String> list = new List();
    int limit = _rdLimit(length);
    while (_rdIdx < limit)
      list.add(_readString(maxItemLength, delimiter));
    return list;
    */
  }

  List<int> readIntegerList(int maxLength,
                                {int maxItemLength: 12,
                                 int delimiter: $BACKSLASH}) {
    List list = new List();
    int limit = _rdLimit(maxLength);
    while (_rdIdx < limit) {
      int temp = readInteger(min(limit - _rdIdx, maxItemLength));
      list.add(temp);
      skipChars(delimiter, 1);
    }
    return list;
  }

  /**
     * Read an integer from [this] ByteBuf.
     *
     * Note: The integer might have leading or training spaces, which are ignored.
     *       When writing the numbers as code units no leading or
     *       trailing whitespace (SPACE only) should be written.
     */
  int readInteger(int maxLength) {
    int limit = _rdLimit(maxLength);
    skipChars($SPACE, limit - _rdIdx);
    int n = _readIntegerPart(limit);
    skipChars($SPACE, limit - _rdIdx);
    skipChars($NULL, limit - _rdIdx);
    return n;
  }

  /**
   * Check for leading '+' or '-' and return +1 or -1.
   */

  int _readSign() {
    int char = peek();
    if (char == $MINUS) {
      _rdIdx++;
      return -1;
    } else if (char == $PLUS) {
      _rdIdx++;
      return 1;
    } else return 1;
  }

  /**
     * Read the integer part of a number (integer or  decimal) number from a Uint8List.
     *
     * Note: The number (integer or decimal) might have leading or training
     *       spaces.  When writing the numbers as code units no leading or
     *       trailing whitespace should be written.
     */

  int _readIntegerPart(int limit) {
    int value = 0;
    int sign = _readSign();
    while(_rdIdx < limit) {
      var char = _peek();
      if ((char >= $DIGIT_0) && (char <= $DIGIT_9)) {
        value = (value * 10) + (char - $DIGIT_0);
        _rdIdx++;
      } else break;
    }
    return sign * value;
  }

  /**
     * Read the fractional part of a decimal number
     *
     * The fractional part MUST start with a $PERIOD (decimal point).
     */
  num _readFractionalPart(int length) {
    num base = 1;
    num value = 0.0;
    if (_peek() != $PERIOD) return 0;
    _rdIdx++;
    while (_rdIdx < length) {
      int char = _peek();
      if ((char >= $DIGIT_0) && (char <= $DIGIT_9)) {
        base *= 10;
        value = (value + ((char - $DIGIT_0) / base));
        _rdIdx++;
      } else {
        break;
      }
    }
    return value;
  }

  num _readExponentPart(int limit) {
    num value = 1;
    int char = _peek();
    if ((char == $E) || (char == $E)) {
      _rdIdx++;
      value = _readIntegerPart(limit);
    } else return 1;
    return pow(10, value);
  }

  /**
    * Reads the bytes as characters and reads a decimal number starting at
    * _rdIdx.  The number has the format [sign]int[.int][Eint].  [maxLength]
    * specifies the maximum length of the decimal string. This method should not
    * read more than [maxLength] characters.
    */
  //TODO make sure that length is not exceeded
  num readDecimal([int maxLength = null]) {
    int limit = _rdLimit(maxLength);
    skipChars($SPACE, limit);
    int i = _readIntegerPart(limit);
    num f = 0;
    num e = 1;
    if (limit != _rdIdx) f = _readFractionalPart(limit);
    if (limit != _rdIdx) e = _readExponentPart(limit);
    skipChars($SPACE, limit - _rdIdx);
    skipChars($NULL, limit - _rdIdx);

    if (i < 0) {
      f = (i - f);
    } else {
      f = (i + f);
    }
    return pow(f, e);
  }

  List<num> readDecimalList(int length,
                           {int delimiter: $BACKSLASH,
                            int maxItemLength: -1}) {
    List<num> list = new List();
    int limit = _rdLimit(length);
    while (_rdIdx < limit) {
      num n = readDecimal(limit - _rdIdx);
      list.add(n);
      skipChars($BACKSLASH, 1);
    }
    return list;
  }

  /*
  /**
   * Read a Universally Unique Identifier (UUID)
   * See http://docs.oracle.com/javase/7/docs/api/java/util/UUID.html
   */
  List<Uuid> readUuidList(int length) {
    List list = new List();
    for(var i = 0; i < length; i++) {
      list.add(readUuid());
    }
    return list;
  }

  Uint8List readUuid() => uuid.parse(readString(16));
*/

  List<String> readUuidList(int length) {
    List list = new List();
    for(var i = 0; i < length; i++) {
      list.add(readString(16));
    }
    return list;
  }

  /**
   * Read a OSI Object Identification UID
   * Based on the OSI Object Identification (numeric form) as defined by
   * the ISO 8824 standard.  See DICOM PS3.5 Section 9 (page 61 in .pdf)
   */
  //TODO test
  //TODO should this be returning a UID object rather than a string?
  String readUid(length) {
    String uidStr = readString(length);
//    UidUtils.isValid(uidStr);
    return uidStr;
  }

  //TODO finish and test
  List<String> readUidList(length) {
    List<String> list = new List();
    int limit = _rdIdx + length;
    for (var i = _rdIdx; i < limit; i++) {
      if (_uint8List[i] == $BACKSLASH) {
        list.add(readUid(i-_rdIdx));
        _rdIdx = i+1;
      }
    }
    if (_rdIdx < limit) {
      list.add(readUid(limit-_rdIdx));
      _rdIdx = limit;
    }
    return list;
  }

  Uri readUri(int length) => Uri.parse(readString(length));

  /// Binary Readers

  /// Int8, Int8List, Uint8, and Uint8List
  static const _int8Size = 1;

  // size in bytes

  int readInt8() {
    _chkRdIdx(_int8Size);
    var val = _bd.getInt8(_rdIdx);
    _rdIdx += _int8Size;
    return val;
  }

  //TODO verify that this makes a copy of the buffer

  Int8List readInt8List(int length, [bool copy = true]) {
    _checkRdLen(length, _int8Size);
    Int8List val = new Int8List.view(_bd.buffer, _rdIdx, length);
    _rdIdx += length;
    return new Int8List.fromList(val);
  }

  int readUint8() {
    _chkRdIdx(_int8Size);
    var val = _bd.getUint8(_rdIdx);
    _rdIdx += _int8Size;
    return val;
  }

  Uint8List readUint8List(int length, [bool copy = true]) {
    _checkRdLen(length, _int8Size);
    Uint8List val = readView(length);
    return val;
  }

  /// Int16, Int16List, Uint16, and Uint16List
  static const int _int16Size = 2;

  // Size in bytes

  int readInt16() {
    _chkRdIdx(_int16Size);
    var val = _bd.getInt16(_rdIdx, _endianness);
    _rdIdx += _int16Size;
    return val;
  }

  Int16List readInt16List(int length) {
    int size = _checkRdLen(length, _int16Size);
    Int16List list = new Int16List(size);
    for (int i = 0; i < size; i++) {
      list[i] = readInt16();
    }
    return list;
  }

  int readUint16() {
    _chkRdIdx(_int16Size);
    var val = _bd.getUint16(_rdIdx, _endianness);
    _rdIdx += _int16Size;
    return val;
  }

  int peekUint16([int temporarySkip = 0]) {
    _chkRdIdx(_int16Size);
    int value = _bd.getUint16(_rdIdx + temporarySkip, _endianness);
    return value;
  }

  Uint16List readUint16List(int length) {
    int size = _checkRdLen(length, _int16Size);
    Uint16List list = new Uint16List(size);
    for (int i = 0; i < size; i++) {
      list[i] = readUint16();
    }
    return list;
  }

  /// Int32, Uint32List, Uint32, and Uint32List
  static const int _int32Size = 4;

  //Size in bytes

  int readInt32() {
    _chkRdIdx(_int32Size);
    var val = _bd.getInt32(_rdIdx, _endianness);
    _rdIdx += _int32Size;
    return val;
  }

  Int32List readInt32List(int length) {
    int size = _checkRdLen(length, _int32Size);
    Int32List list = new Int32List(size);
    for (int i = 0; i < size; i++) {
      list[i] = readInt32();
    }
    return list;
  }

  int readUint32() {
    _chkRdIdx(_int32Size);
    var val = _bd.getUint32(_rdIdx, _endianness);
    _rdIdx += _int32Size;
    return val;
  }

  Uint32List readUint32List(int length) {
    int size = _checkRdLen(length, _int32Size);
    Uint32List list = new Uint32List(size);
    for (int i = 0; i < size; i++) {
      list[i] = readUint32();
    }
    return list;
  }

  /// Int64, Int64List, Uint64, and Uint64List
  static const int _int64Size = 8;

  //Size final bytes

  int readInt64() {
    _chkRdIdx(_int64Size);
    var val = _bd.getInt64(_rdIdx, _endianness);
    _rdIdx += _int64Size;
    return val;
  }

  Int64List readInt64List(int length) {
    int size = _checkRdLen(length, _int64Size);
    Int64List list = new Int64List(size);
    for (int i = 0; i < size; i++) {
      list[i] = readInt64();
    }
    return list;
  }

  int readUint64() {
    _chkRdIdx(_int64Size);
    var val = _bd.getUint64(_rdIdx, _endianness);
    _rdIdx += _int64Size;
    return val;
  }

  Uint64List readUint64List(int length) {
    int size = _checkRdLen(length, _int64Size);
    Uint64List list = new Uint64List(size);
    for (int i = 0; i < size; i++) {
      list[i] = readUint64();
    }
    return list;
  }

  /// [Float32] single values and lists
  static const _float32Size = 4;

  //Size in bytes

  num readFloat32() {
    _chkRdIdx(_float32Size);
    var val = _bd.getFloat32(_rdIdx, _endianness);
    _rdIdx += _float32Size;
    return val;
  }

  Float32List readFloat32List(int length) {
    int size = _checkRdLen(length, _float32Size);
    Float32List list = new Float32List(size);
    for (int i = 0; i < size; i++) {
      list[i] = readFloat32();
    }
    return list;
  }

  /// Float64 and Float64List
  static const int _float64Size = 8;

  num readFloat64() {
    _chkRdIdx(_float64Size);
    var val = _bd.getFloat64(_rdIdx, _endianness);
    _rdIdx += _float64Size;
    return val;
  }

  Float64List readFloat64List(int length) {
    int size = _checkRdLen(length, _float64Size);
    Float64List list = new Float64List(size);
    for (int i = 0; i < size; i++) {
      list[i] = readFloat64();
    }
    return list;
  }

  ///Writer Utilities
  ///Methods for managing public write methods
  void operator []=(int _wrIdx, int value) {
    _uint8List[_chkWrIdx(_wrIdx)] = _inUint8Range(value);
  }

  int _inUint8Range(int value) {
    return ((value < 0) || (value >= 256)) ? _rangeError(
        'Value:$value out of range 0..256.') : value;
  }

  int get wrIndex => _wrIdx;
  void set wrIndex(int i) { _wrIdx = _chkWrIdx(i); }
  int get wrRemaining => (_bd.lengthInBytes - _wrIdx);

  int _chkWrIdx(int index) {
    return ((index < _wrIdx) || (index >= capacity)) ? _rangeError(
        'Write index out of range $_wrIdx..$_capacity.') : index;
  }

  int _chkWrLen(int length, [int elementSize = 1, int maxLength = -1]) {
    int len = length * elementSize;
    return ((len > wrRemaining) || (len > maxLength))
              ? _rangeError('Write length exceeds remaining=$wrRemaining')
              : length;
  }

  /// String Writers
  //TODO look at how this is done in Convert
  void writeString(String s, {int maxLength: -1}) {
    int limit = _chkWrLen(s.length, 1, maxLength);
    for(var i = 0; i < limit; i++) {
      write(s.codeUnitAt(i));
    }
  }

  void writeStringList(List<String> l, {int maxItemLength: -1}) {
    for(var i = 0; i < l.length; i++)
      for (var j = 0; j < l[i].length; j++)
        write(l[i].codeUnitAt(j));
  }

  void write(int i) => writeUint8(i);

  /// Binary Writers
  //TODO do we need the _inRange check?  will setInt8 signal the error?
  void writeInt8(int i) {
    _bd.setInt8(_wrIdx, _chkWrLen(i, 1));
    _wrIdx++;
  }

  void writeInt8List(Int8List l) {
    int limit = _chkWrLen(l.length, 1);
    for(var i = 0; i < limit; i++) writeInt8(i);
  }

  void writeUint8(int i) {
    _bd.setUint8(_wrIdx, _chkWrLen(i));
    _wrIdx++;
  }

  void writeUint8List(Uint8List l) {
    int limit = _chkWrLen(l.length, 1);
    for(var i = 0; i < l.length; i++) writeUint8(i);
  }

  void writeInt16(int i) {
    _bd.setInt16(_wrIdx, i, _endianness);
    _wrIdx += 2;
  }

  void writeInt16List(Uint8List l) {
    int limit = _chkWrLen(l.length, 2);
    for(var i = 0; i < l.length; i++) writeInt16(i);
  }

  void writeUint16(int i) {
    _bd.setUint16(_wrIdx, i, _endianness);
    _wrIdx += 2;
  }

  void writeUint16List(Uint8List l) {
    int limit = _chkWrLen(l.length, 2);
    for(var i = 0; i < l.length; i++) writeInt16(i);
  }

  void writeInt32(int i) {
    _bd.setInt32(_wrIdx, i, _endianness);
    _wrIdx += 4;
  }

  void writeUint32(int i) {
    _bd.setUint32(_wrIdx, i, _endianness);
    _wrIdx += 4;
  }

  void writeInt64(int i) {
    _bd.setInt64(_wrIdx, i, _endianness);
    _wrIdx += 8;
  }

  void writeUint64(int i) {
    _bd.setUint64(_wrIdx, i, _endianness);
    _wrIdx += 8;
  }

  ///Error Generation
  void charsetError(int char) {
    String s = ASCII.decode([char]);
    throw new Exception('ValueField has invalid character = $char= $s');
  }

  void _createError(int start, int end) {
    var msg = "Invalid arguments to ByteBuf: $this, $start..$end";
    throw new ArgumentError(msg);
  }

  /**
   * RangeErrors indicating an invalid index into ByteBuf.
   */
  void _rangeError(String msg) {
    throw new RangeError(msg);
  }

  /**
   * Signal an error occured while parsing the data in this ByteBuf.
   */

  void parseError(String msg) {
    throw new Exception("ByteBuf Parse Error: $msg");
  }

  String get debug =>
      "BB: rdIdx=$_rdIdx, wrIdx=$_wrIdx, rdRemaining=$rdRemaining";

  String toString() => "ByteBuf($hashCode: BD=$_bd R=$_rdIdx, W=$_wrIdx)";
}